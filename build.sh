#!/bin/bash
#============================================================================
#title          :Unlock Project
#description    :Project initialization application
#author         :Jasper Koehorst
#date           :2020
#version        :0.9.9
#============================================================================
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

git -C $DIR pull

# Making sure the ontology is available
# $DIR/../ontology/build.sh

# ////////////////////////////////////////////////////////////////////////////////////
# Jargon dependency
# ////////////////////////////////////////////////////////////////////////////////////

wget -nc https://github.com/DICE-UNC/jargon/releases/download/4.3.0.2-RELEASE/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar -O $DIR/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar
mvn install:install-file -Dfile=$DIR/jargon-core-4.3.0.2-RELEASE-jar-with-dependencies.jar -DgroupId=jargon -DartifactId=core -Dversion=4.3.0.2 -Dpackaging=jar

# ////////////////////////////////////////////////////////////////////////////////////
# // Download http://download.systemsbiology.nl/ngtax/dev/NGTax-2.0.89.jar for amplicon libraries / demultiplexing
# ////////////////////////////////////////////////////////////////////////////////////

wget -nc http://download.systemsbiology.nl/ngtax/dev/NGTax-2.2.4.jar
mvn install:install-file -Dfile=$DIR/NGTax-2.2.4.jar -DgroupId=nl.wur.ssb -DartifactId=ngtax -Dversion=2.0.104 -Dpackaging=jar

# ////////////////////////////////////////////////////////////////////////////////////
# // UNLOCK API
# ////////////////////////////////////////////////////////////////////////////////////
wget -nc http://download.systemsbiology.nl/unlock/UnlockOntology.jar -O $DIR/UnlockOntology.jar
mvn install:install-file -Dfile=$DIR/UnlockOntology.jar -DgroupId=nl.munlock -DartifactId=unlockapi -Dversion=1.0.1 -Dpackaging=jar

# ////////////////////////////////////////////////////////////////////////////////////
if [ "$1" == "test" ]; then
	shift;
	gradle build -b "$DIR/build.gradle" $@
else
	echo "Skipping tests, run './install.sh test' to perform tests"
	gradle build -b "$DIR/build.gradle" -x test $@
fi

cp $DIR/build/libs/*jar $DIR/

# java -jar $DIR/iRODSManager.jar
