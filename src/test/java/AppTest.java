import nl.munlock.datamanager.App;
import nl.munlock.datamanager.ExecCommand;
import nl.munlock.datamanager.irods.Connection;
import nl.wur.ssb.RDFSimpleCon.Util;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicParserSettings;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.connection.*;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.pub.io.IRODSFileFactory;
import org.irods.jargon.core.query.*;
import org.junit.Before;
import org.junit.Test;

import java.io.*;
import java.lang.reflect.Field;
import java.util.*;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;

public class AppTest {

    @Test
    public void testLocal() throws Exception {
        String turtle;
//        turtle = "/unlock/landingzone/projects/PRJ_E2BN/INV_LORA/LORA_unlock_2021_excl_samples.ttl";
        turtle = "/unlock/landingzone/projects/PRJ_UNLOCK/INV_DHB_UNLOCK/DHB_TUD_metadata.ttl";
        turtle = "/unlock/landingzone/projects/PRJ_UNLOCK/INV_CAMI/UNLOCK_CAMI.ttl";
        turtle = "/unlock/landingzone/projects/PRJ_IBISBA/INV_COCULTURE/IBISBA-COCULTURE.ttl";

        String[] args = {
                "-turtle", turtle,
                "-donotcheck",
//                "-debug",
                "-enaBrowserTools", "/Users/jasperk/gitlab/enaBrowserTools",
//                "-conversionTools","/Users/jasperkoehorst/GitLab/m-unlock/sync/sapp/Conversion.jar",
//                "-ngtaxTools", "NGTax-2.1.63.jar",
                "-sratoolkit", "/Users/jasperk/Downloads/sratoolkit.2.11.1-mac64"
        };

        // Try again if not successful
        while (true) {
            App.main(args);
            break;
        }
    }

    @Test
    public void testTurtles() throws Exception {
        // FAILURES
        // "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/ERP018614.ttl"
        String turtles[] = {
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007222.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/ERP020473.ttl",
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007221.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP277455.ttl", // DONE
                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP005906.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007218.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007099.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP110600.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP223742.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007220.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/ERP122530.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP167410.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP114403.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP230208.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/ERP010962.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP119008.ttl",
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP083099.ttl",
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP148840.ttl",
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP006580.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/DRP007219.ttl", // DONE
//                "/unlock/landingzone/projects/P_RIVM/I_SRA_Amplicon_2_function/SRP252519.ttl"
        };
        while (true) {
            for (String turtle : turtles) {
                String[] args = {
                        "-turtle", turtle,
                        // "-donotcheck", // "-debug",
                        "-enaBrowserTools", "/Users/jasperk/gitlab/enaBrowserTools",
//                        "-conversionTools","/Users/jasperkoehorst/GitLab/m-unlock/sync/sapp/Conversion.jar",
//                        "-ngtaxTools", "NGTax-2.1.63.jar",
                        "-sratoolkit", "/Users/jasperk/Downloads/sratoolkit.2.10.9-mac64"
                };
                try {
                    App.main(args);
                } catch (Exception e) {
                    System.err.println(e);
                }
            }
            return;
        }
    }

    @Before
    public void config() throws FileNotFoundException {
        File file = new File("environment.config");
        if (file.exists()) {
            System.out.println("Parsing test config file " + file);
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                System.err.println(">" + line + "<");
                String[] lineSplit = line.split("\\s+");
                setEnv(lineSplit[0], lineSplit[1]);
            }
        }
    }

    public void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }

    @Deprecated
    @Test
    public void testPrivate() throws Exception {
        String turtle = "/unlock/landingzone/projects/P_EXPLODIV/EXPLODIV_BIOGAS.ttl";
        String[] args = {
                "-turtle", turtle,
                "-debug",
                "-ngtaxTools", "NGTax-2.0.104.jar",
                "-enaBrowserTools", "/Users/jasperkoehorst/GitLab/enaBrowserTools"};
        App.main(args);
    }

    @Test
    public void testChecksum() throws Exception {
        // Initialize account object
        IRODSAccount irodsAccount = IRODSAccount.instance(
                System.getenv().get("irodsHost"),
                Integer.parseInt(System.getenv().get("irodsPort")),
                System.getenv().get("irodsUserName"),
                System.getenv().get("irodsPassword"),
                "",
                System.getenv().get("irodsZone"),
                "");

        SettableJargonPropertiesMBean jargonProperties = new SettableJargonProperties();
        jargonProperties.setChecksumEncoding(ChecksumEncodingEnum.SHA256);

        // Disables SSL
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        clientServerNegotiationPolicy.setSslNegotiationPolicy(CS_NEG_REFUSE);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        IRODSFileSystem irodsFileSystem = IRODSFileSystem.instance();

        IRODSAccessObjectFactory accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

        // Change iRODS checksum type to sha256
        IRODSProtocolManager irodsConnectionManager = IRODSSimpleProtocolManager.instance();
        IRODSSession irodsSession = IRODSSession.instance(irodsConnectionManager);
        SettableJargonPropertiesMBean settableProperties = new SettableJargonProperties(irodsSession.getJargonProperties());
        settableProperties.setChecksumEncoding(ChecksumEncodingEnum.MD5);
        irodsSession.setJargonProperties(settableProperties);
        accessObjectFactory.setIrodsSession(irodsSession);

        // Evaluate checksum
//        ChecksumEncodingEnum checksumEncoding = accessObjectFactory.getJargonProperties().getChecksumEncoding();
//        System.err.println("Checksum " + checksumEncoding.getTextValue());

//        AuthResponse x = accessObjectFactory.authenticateIRODSAccount(irodsAccount);
//        log.info(x.getAuthMessage());

        IRODSFileFactory fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);


        // Do the checksum
        String file = "/tempZone/projects/P_IRODSRUNNER/I_Test_investigation/S_BABY_X/amplicon/A_amp1bx/unprocessed/G76494_R1_001.fastq.gz";
        IRODSFile irodsFile = fileFactory.instanceIRODSFile(file);
//        System.out.println("Checksum " + irodsFileSystem.getIRODSAccessObjectFactory().getJargonProperties().getChecksumEncoding());
        DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(irodsAccount);
        ChecksumValue checksum = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
//        System.out.println(checksum.getHexChecksumValue());
//        System.out.println(checksum.getChecksumEncoding());
//        System.out.println(checksum);
    }

    @Deprecated
    @Test
    public void testPath() throws Exception {
        String fileName = "NG-23723_2020_0003_lib373519_6647_2_2.fastq.gz";
        String project = "";
        Connection connection = new Connection();

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "%landingzone%");
        // IF pattern does not match ENA... add project search field?
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "%landingzone%" + project + "%");

        // Skips trash folder
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "%/trash/%");

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.EQUAL, fileName);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit to 2 but should only return 1 file
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(2);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            throw new JargonQueryException("No results found with " + fileName);
        }

        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            // System.err.println(path);
        }
    }

    @Test
    public void testSubdir() throws Exception {
        boolean recursive = true;
        String fileName = "L8D21_amplicon_r.fastq.gz";
        Collection files = FileUtils.listFiles(new File("."), null, recursive);

        for (Iterator iterator = files.iterator(); iterator.hasNext(); ) {
            File file = (File) iterator.next();
            if (file.getName().equals(fileName)) {
                // System.out.println(file.getAbsolutePath());
            }
        }
    }

    @Test
    public void graphDB() throws IOException {
        // Initialise endpoint
        InputStream inputStream = Util.getResourceFile("graphdb/repo-config.ttl");
        StringWriter writer = new StringWriter();
        IOUtils.copy(inputStream, writer, "UTF-8");
        String theString = writer.toString();

        PrintWriter printWriter = new PrintWriter("repo-config.ttl");
        printWriter.println(theString);
        printWriter.close();

        // Command
        String graphdb = "http://localhost:7200/rest/repositories";
        String command = "curl -X POST " + graphdb + " -H 'Content-Type: multipart/form-data' -F \"config=@repo-config.ttl\"";
        ExecCommand execCommand = new ExecCommand(command);
        if (execCommand.getExit() > 0) {
            System.err.println("COMMAND FAILED");
            System.err.println(execCommand.getOutput());
        } else {
            System.err.println(execCommand.getOutput());
        }
    }

    @Test
    public void testRDF() throws IOException {
//        org.eclipse.rdf4j.rio.RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
//        rdfParser.getParserConfig().set(BasicParserSettings.VERIFY_URI_SYNTAX, false);
//        InputStream inputStream = new FileInputStream(new File("test.ttl"));
//        rdfParser.parse(inputStream, "");

        // If it is a TTL file perform a RIOT analysis
    }
}
