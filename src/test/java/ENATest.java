import nl.munlock.datamanager.App;
import nl.munlock.datamanager.irods.Connection;
import nl.munlock.datamanager.irods.Data;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.lang3.StringUtils;
import org.jermontology.ontology.JERMOntology.domain.*;
import org.junit.Test;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;

import java.io.File;
import java.io.PrintWriter;
import java.util.HashSet;

public class ENATest {

    /**
     * This will create a SRA toolkit download file for parallel retrieval of data
     */
    @Test
    public void enaTest() throws Exception {
        String turtle = "/unlock/landingzone/projects/P_COMP-TAX-FUNC/I_COMP-MGNIFY/mgnify_rhizosphere_selected_modified.ttl";
        Connection connection = new Connection();
        Data.downloadFile(connection, new File(turtle));
        Domain domain = new Domain("file://." + turtle);
        // Obtain project
        Iterable<ResultLine> resultLines = domain.getRDFSimpleCon().runQuery("getProjects.txt", true);
        for (ResultLine resultLine : resultLines) {
            Project project = domain.make(Project.class, resultLine.getIRI("project"));
            PrintWriter printWriter = new PrintWriter(new File(project.getIdentifier() + "_sra.sh"));
            HashSet<String> identifiers = new HashSet<>();
            for (Investigation investigation : project.getAllHasPart()) {
                // /landingzone/projects/P_COMP-TAX-FUNC/data/sra/AmpliconAssay/ERR1905506_1.fastq.gz,11649134,1635832597000
                String store = "/unlock/landingzone/projects/PRJ_" + project.getIdentifier() + "/data/sra";
                for (Study study : investigation.getAllHasPart()) {
                    for (observation_unit observation_unit : study.getAllHasPart()) {
                        for (Sample sample : observation_unit.getAllHasPart()) {
                            for (Assay assay : sample.getAllHasPart()) {
                                String assayType = assay.getClassTypeIri().replaceAll(".*/", "");
                                for (Data_sample data_sample : assay.getAllHasPart()) {
                                    String fileName = data_sample.getName();
                                    String path = store + "/" + assayType + "/" + fileName;
                                    if (!connection.fileFactory.instanceIRODSFile(path).exists()) {
                                        if (fileName.matches("[DES]RR[0-9]+_[12].fastq.gz")) {
                                            String sratoolkit = "/unlock/infrastructure/binaries/sra/sratoolkit/bin/fasterq-dump";
                                            sratoolkit = "/Users/jasperk/Downloads/sratoolkit.2.11.1-mac64/bin/fasterq-dump";
                                            String identifier = fileName.split("_")[0];
                                            if (!identifiers.contains(identifier)) {
                                                String[] command = {sratoolkit, "--force", "-p", identifier};
                                                printWriter.println(StringUtils.join(command, " ") + " && gzip " + identifier + "_* && mv " + identifier + "_* " + store + "/" + assayType);
                                            }
                                            identifiers.add(identifier);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            printWriter.close();
        }
    }
}
