package nl.munlock.datamanager;

import nl.munlock.datamanager.irods.Connection;
import nl.munlock.datamanager.irods.IRODS;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import java.io.File;

/**
 * Project initializer which converts a metadata excel sheet obtained from
 *
 */
public class App {
    public static boolean debug = false;
    private static final Logger log = Generic.getLogger(App.class, debug);
    public static Connection connection;
    public static CommandOptions commandOptions;

    public static void main(String[] args) throws Exception {

        commandOptions = new CommandOptions(args);

        if (commandOptions.debug)
            debug = true;

        if (debug) {
            log.debug("DEBUG ACTIVATED!");
        }

        if (App.debug) {
            org.apache.log4j.Logger.getLogger("org").setLevel(Level.ALL);
        } else {
            org.apache.log4j.Logger.getLogger("org").setLevel(Level.OFF);
        }

        // Some checks
        if (!new File(commandOptions.ngtax).exists()) {
            throw new Exception("NGTax not found on given path");
        }

        // Establishing an iRODS connection
        connection = new Connection();
        log.info("Authenticated using " + connection.irodsAccount.getUserName());

        IRODS.make();
    }
}
