package nl.munlock.datamanager;

import nl.munlock.datamanager.irods.Data;
import nl.munlock.datamanager.irods.Connection;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QuerySolution;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static nl.munlock.datamanager.irods.IRODS.checkFileExistsOnIrods;

public class GenomeSync {
    private static final Logger log = Generic.getLogger(GenomeSync.class, false);

    public static String sync_ena_gca(Connection connection, CommandOptions commandOptions, String fileName) throws Exception {
        // Perform iRODS check if it exists
        String irodsFilePath = checkFileExistsOnIrods(connection, fileName);
        // if not exists perform genome sync and store in /tempZone/references/genomes/
        if (irodsFilePath == null) {

            // Execute enabrowsertools for genome data downloading in fasta or embl format
            File gcaFolder = new File("TEMP_GCA/" + fileName);

            String command = "python3 " + commandOptions.enaBrowserTools + "/python3/enaDataGet.py -f embl -exp -d TEMP_GCA " + fileName;

            ExecCommand execCommand = new ExecCommand(command);
            if (execCommand.getExit() > 0)
                throw new IOException(execCommand.getError());

            // Merge all the correct files in the folder to a single GZIP file
            ArrayList<File> keep = new ArrayList<>();
            if (gcaFolder.listFiles() != null) {
                for (File file : gcaFolder.listFiles()) {
                    if (!file.getName().endsWith("xml") && !file.getName().endsWith("txt") && !file.getName().startsWith(".")) {
                        keep.add(file);
                    }
                }

                // Stream to GCA in compressed format
                File referenceEMBLFile = new File(gcaFolder + "/" + fileName + ".embl.gz");
                if (!referenceEMBLFile.exists()) {
                    FileOutputStream output = new FileOutputStream(referenceEMBLFile);
                    Writer writer = new OutputStreamWriter(new GZIPOutputStream(output), "UTF-8");
                    for (File fileKeep : keep) {
                        InputStream fileIn;
                        if (fileKeep.getName().endsWith(".gz")) {
                            fileIn = new GZIPInputStream(new FileInputStream(fileKeep));
                        } else {
                            fileIn = new FileInputStream(fileKeep);
                        }

                        IOUtils.copy(fileIn, writer);
                        fileIn.close();
                    }
                    writer.close();
                }

                File xmlFile = new File(referenceEMBLFile.getAbsolutePath().replaceAll(".embl.gz$", ".xml"));
                Scanner scanner = new Scanner(xmlFile);
                ArrayList<String> lineage = new ArrayList<>();
                while (scanner.hasNextLine()) {
                    String line = scanner.nextLine();
                    if (line.contains("<TAXON_ID>")) {
                        String taxon = line.split(">")[1].split("<")[0];
                        log.info("TAXON DETECTED: " + taxon);
                        lineage = getLineage(taxon);
                        log.info("LINEAGE: " + StringUtils.join(lineage,"/"));
                    }
                }

                // out files
                File turtleFile = new File(referenceEMBLFile.getName().replaceAll(".embl.gz","") + ".ttl");
                File gzipTTLFile = new File(referenceEMBLFile.getName().replaceAll(".embl.gz","") + ".ttl.gz");

                // Perform the conversion to RDF
                String[] args = new String[]{"java", "-jar", commandOptions.conversion, "-embl2rdf",
                        "-i", referenceEMBLFile.getAbsolutePath(),
                        "-output", turtleFile.getAbsolutePath(),
                        "-id", referenceEMBLFile.getName().replaceAll(".embl.gz","")
                };

                // nl.wur.ssb.conversion.App.main(args);
                log.info("Starting conversion of " + referenceEMBLFile.getAbsolutePath() + " to " + referenceEMBLFile.getName().replaceAll(".embl.gz",""));
                execCommand = new ExecCommand(args);
                if (execCommand.getExit() == 0) {
                    log.info("Conversion successful");
                    Generic.compressGZIP(turtleFile, gzipTTLFile);
                } else {
                    log.error("Conversion failed");
                    log.error(execCommand.getError());
                }

                // Upload with lineage information
                if (lineage.size() > 0) {
                    String lineagePath = "/" + connection.irodsAccount.getZone() +
                            "/references/genomes/" + StringUtils.join(lineage,"/").replaceAll(" +","_") +
                            "/" + xmlFile.getName().replaceAll("\\.[0-9]+.*", "");
                    lineagePath = lineagePath.toLowerCase();
                    log.info(lineagePath);
                    connection.fileFactory.instanceIRODSFile(lineagePath).mkdirs();
                    Data.uploadIrodsFile(connection, xmlFile, new File(lineagePath +"/" + xmlFile.getName()));
                    Data.uploadIrodsFile(connection, referenceEMBLFile, new File(lineagePath +"/" + referenceEMBLFile.getName()));
                    Data.uploadIrodsFile(connection, gzipTTLFile, new File(lineagePath +"/" + gzipTTLFile.getName()));
                    return lineagePath;
                }

                // Acquire taxon id?

                // Get lineage

                // Upload EMBL file to the correct path...?


                // Destination folder
                // String destinationFolder = "/tempZone/references/RDF/GBOL/" + entry.getENA().replaceAll(arguments.output.toString() + "/ENA", "").replaceAll("/$", "") + "/" + entry.getAssemblyAccession().replaceAll("\\.[0-9]+$", "") + "/";
                // Create directory
                // connection.fileFactory.instanceIRODSFile(destinationFolder).mkdirs();

            }
        }

        // CREATE YAML FILE

        // Destination path needed and cwl filename

        // Upload to iRODS

//        String filePath = "";
        return null;
    }

    static ArrayList<String> getLineage(String taxonid) {


        String queryString =
                "PREFIX up:<http://purl.uniprot.org/core/> PREFIX taxon:<http://purl.uniprot.org/taxonomy/> " +
                        "PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>" +
                        "SELECT DISTINCT ?parentClass ?rank ?className " +
                        "WHERE" + "       {" +
                        " VALUES ?taxon{taxon:" + taxonid + "}" +
                        "?taxon up:scientificName ?name." +
                        "{"
                        + "  ?taxon up:rank ?rank ." + "       ?taxon up:scientificName ?className ." + "       BIND(?taxon AS ?parentClass)" + "   }" + "   union" + "   {"
                        + "  ?taxon rdfs:subClassOf+ ?parentClass ." + "       ?parentClass up:rank ?rank ." + "       ?parentClass up:scientificName ?className ." + "   }" + "}";


        QueryExecution qexec = QueryExecutionFactory.sparqlService("https://sparql.uniprot.org/sparql/", queryString);

        org.apache.jena.query.ResultSet results = qexec.execSelect();

        ArrayList<String> lineage = new ArrayList<>();

        lineage.add("Superkingdom");
        lineage.add("Phylum");
        lineage.add("Class");
        lineage.add("Order");
        lineage.add("Family");
        lineage.add("Genus");
        lineage.add("Species");

        try {
            results.hasNext();
        } catch (NullPointerException e) {
            log.info("Nullpointer for results with taxid: " + taxonid);
            return lineage;
        }

        if (!results.hasNext()) {
            log.info("No results for " + taxonid);
        }

        while (results.hasNext()) {
            QuerySolution result = results.next();
            String rank = result.get("rank").toString();
            String name = result.get("className").toString();
            if (rank.contains("Superkingdom")) {
                lineage.set(0, name);
            }
            if (rank.contains("Phylum")) {
                lineage.set(1, name);
            }
            if (rank.contains("Class")) {
                lineage.set(2, name);
            }
            if (rank.contains("Order")) {
                lineage.set(3, name);
            }
            if (rank.contains("Family")) {
                lineage.set(4, name);
            }
            if (rank.contains("Genus")) {
                lineage.set(5, name);
            }
            if (rank.contains("Species")) {
                lineage.set(6, name);
            }
        }
        qexec.close();
        return lineage;
    }
}
