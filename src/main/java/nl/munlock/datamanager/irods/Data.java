package nl.munlock.datamanager.irods;

import nl.munlock.datamanager.Generic;
import nl.munlock.datamanager.App;
import nl.munlock.datamanager.ExecCommand;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.helpers.BasicParserSettings;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactory;
import org.irods.jargon.core.checksum.LocalChecksumComputerFactoryImpl;
import org.irods.jargon.core.checksum.SHA256LocalChecksumComputerStrategy;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.DataTransferOperations;
import org.irods.jargon.core.pub.IRODSGenQueryExecutor;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

import java.io.*;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static nl.munlock.datamanager.App.commandOptions;
import static nl.munlock.datamanager.Generic.getSHA256;
import static nl.munlock.ontology.domain.FileType.*;

public class Data {

    private static final Logger log = Generic.getLogger(Data.class, App.debug);

    /**
     * Uploading a local file to iRODS
     *
     * @param connection the authentication object to access irods
     * @param localFile  a local file that will be uploaded to the irods location
     * @param remoteFile the path to the destination
     * @throws JargonException
     * @throws FileNotFoundException
     */
    public static void uploadIrodsFile(Connection connection, File localFile, File remoteFile) throws JargonException, IOException {
        log.info("Uploading " + localFile + " to " + remoteFile);

        if (!localFile.exists()) {
            log.error("Local file " + localFile + " does not exists... cancelling upload");
            return;
        }

        if (localFile.getName().endsWith(".ttl")) {
            log.debug("Validating RDF");
            org.eclipse.rdf4j.rio.RDFParser rdfParser = Rio.createParser(RDFFormat.TURTLE);
            rdfParser.getParserConfig().set(BasicParserSettings.VERIFY_URI_SYNTAX, false);
            InputStream inputStream = new FileInputStream(localFile);
            rdfParser.parse(inputStream, "");
            inputStream.close();
        }


        // Check if file is not an irods location
        IRODSFile irodsFileFinalDestination = connection.fileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());

        ChecksumValue remoteChecksumValue;
        ChecksumValue localChecksumValue;

        if (irodsFileFinalDestination.exists()) {

            // Get local HASH
            LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
            SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
            localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
            // Get remote hash
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFileFinalDestination);

            if (localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                log.debug("Same checksum, not going to overwrite");
                return;
            } else {
                log.debug("Removing remote file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                log.debug("Does not match checksum of " + irodsFileFinalDestination.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                irodsFileFinalDestination.delete();
            }
        }

        if (!connection.fileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).exists()) {
            // Create collection in iRODS
            log.debug("Creating directory: " + remoteFile.getParentFile().getAbsolutePath());
            connection.fileFactory.instanceIRODSFile(remoteFile.getParentFile().getAbsolutePath()).mkdirs();
        }

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        IRODSFile destFile = connection.fileFactory.instanceIRODSFile(remoteFile.getAbsolutePath());
        log.debug(localFile + "\t" + remoteFile);
        dataTransferOperationsAO.putOperation(localFile, destFile, null, null);
    }

    /**
     * Downloads file to the current working directory
     *
     * @param connection the authentication object for iRODS
     * @param download   the file that needs to be downloasded
     * @throws JargonException
     */
    public static void downloadFile(Connection connection, File download) throws JargonException, FileNotFoundException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(download.getAbsolutePath());
        if (!irodsFile.exists()) {
            throw new JargonException("File " + irodsFile + " does not exist");
        }

        DataTransferOperations dataTransferOperationsAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);

        // Create folder directory
        File directory = new File("." + download.getAbsolutePath().replaceAll(download.getName() + "$", ""));
        directory.mkdirs();

        File localFile = new File("." + download);

        if (localFile.exists()) {
            // Perform hash check!
            // Get local HASH
            LocalChecksumComputerFactory factory = new LocalChecksumComputerFactoryImpl();
            SHA256LocalChecksumComputerStrategy localActual = (SHA256LocalChecksumComputerStrategy) factory.instance(ChecksumEncodingEnum.SHA256);
            ChecksumValue localChecksumValue = localActual.computeChecksumValueForLocalFile(localFile.getAbsolutePath());
            // Get remote hash
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            ChecksumValue remoteChecksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
            // If checksum is not the same remove local file
            if (!localChecksumValue.getBase64ChecksumValue().contains(remoteChecksumValue.getBase64ChecksumValue())) {
                log.info("Removing local file location " + localFile + " " + localChecksumValue.getBase64ChecksumValue());
                log.info("Does not match checksum of " + irodsFile.getAbsolutePath() + " " + remoteChecksumValue.getBase64ChecksumValue());
                while (localFile.exists()) {
                    localFile.delete();
                }
            }
        }

        if (!localFile.exists()) {
            // Disables the logger for the transfer as it easily gives thousands of lines...
            log.info("Downloading " + localFile.getName() + " from " + localFile);
            dataTransferOperationsAO.getOperation(irodsFile, localFile, null, null);
        } else {
            log.info("File already exists locally: " + localFile);
        }
    }

    /**
     * Copies the source file to its final destination using the iRODS api
     *
     * @param connection  the iRODS authentication connection
     * @param sourceFileX the source file to be copied
     * @param finalFileX  the desination
     * @throws JargonException
     */
    public static void copyIrodsFile(Connection connection, File sourceFileX, File finalFileX) throws JargonException {
        IRODSFile sourceFile = connection.fileFactory.instanceIRODSFile(sourceFileX.getAbsolutePath());
        IRODSFile finalFile = connection.fileFactory.instanceIRODSFile(finalFileX.getAbsolutePath());

        // If file exists on final destination perform checksum
        if (sourceFile.exists() && finalFile.exists()) {
            log.info("Source and destination file exists, performing checksum tests");
            DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
            ChecksumValue remoteChecksumValue1 = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(sourceFile);
            ChecksumValue remoteChecksumValue2 = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(finalFile);

            if (remoteChecksumValue1.getBase64ChecksumValue().contains(remoteChecksumValue2.getBase64ChecksumValue())) {
                log.info("Same checksum, not going to overwrite " + finalFile);
                return;
            } else {
                // If checksum not the same delete final file and copy...
                log.error("Checksum does not match, cannot trust file, removing... " + finalFile);
                finalFile.delete();
            }
        }
        // Creating the directory
        connection.fileFactory.instanceIRODSFile(finalFile.getParent()).mkdirs();
        // Copy to final location
        log.info("Copying " + sourceFile + " to " + finalFile);
        connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount).copy(sourceFile, finalFile, null, null);

    }

    /**
     * Sets the file type based on the file extension
     *
     * @param dataset rdf object regarding the datafile
     * @throws Exception when file type is not recognised
     */
    public static void findFileType(Data_sample dataset) throws Exception {
        if (dataset.getName().endsWith(".tsv")) {
            dataset.setFileFormat(format_3475);
        } else if (dataset.getName().contains(".fastq")) {
            dataset.setFileFormat(format_1930);
        } else if (dataset.getName().contains(".fq")) {
            dataset.setFileFormat(format_1930);
        } else if (dataset.getName().contains(".fasta")) {
            dataset.setFileFormat(format_1929);
        } else if (dataset.getName().contains(".faa")) {
            dataset.setFileFormat(format_1929);
        } else if (dataset.getName().contains(".fna")) {
            dataset.setFileFormat(format_1929);
        } else if (dataset.getName().contains(".html")) {
            dataset.setFileFormat(format_2331);
        } else if (dataset.getName().contains(".zip")) {
            dataset.setFileFormat(format_3987);
        } else if (dataset.getName().endsWith(".json")) {
            dataset.setFileFormat(format_3464);
        } else if (dataset.getName().endsWith(".biom")) {
            dataset.setFileFormat(format_3464);
        } else if (dataset.getName().endsWith(".log")) {
            dataset.setFileFormat(format_2330);
        } else if (dataset.getName().endsWith(".bam")) {
            dataset.setFileFormat(format_2572);
        } else if (dataset.getName().endsWith(".ttl")) {
            dataset.setFileFormat(format_3255);
        } else if (dataset.getName().endsWith(".jsonld")) {
            dataset.setFileFormat(format_3464);
        } else if (dataset.getName().endsWith("Enter only when data is demultiplexed")) {
            // Skip this for non-demultiplexed data
        } else {
            throw new Exception("File type not recognised: " + dataset.getName());
        }
    }

    public static ArrayList<File> listFiles(Connection connection, IRODSFile irodsFile) throws GenQueryBuilderException, JargonQueryException, JargonException {
        log.info("Listing all files from " + irodsFile.getAbsolutePath());
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // list all files given a basic path with additional "%"
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, irodsFile.getAbsolutePath() + "%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, "%.f%q.gz");

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(999999);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            throw new JargonQueryException("No results found with " + irodsFile.getAbsolutePath());
        }

        ArrayList<File> files = new ArrayList<>();
        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            files.add(new File(path));
        }
        return files;
    }

    public static String findFile(Connection connection, String fileName, ArrayList<File> files) {
        for (File file : files) {
            if (file.getName().equals(fileName)) {
                log.info(file.getName() + " found in irods");
                return file.getAbsolutePath();
            }
        }
        return null;
    }

    public static boolean checkOnlineResources(Connection connection, String fileName, String landingzone, String assayType) throws IOException, JargonException {
        if (fileName.matches("^[DES]RR[0-9]+_[12].fastq.gz")) {
            log.info("ENA number detected");
            // If non gz file exists
            if (new File(fileName).getName().endsWith(".gz") && new File(fileName.replace(".gz","")).exists()) {
                log.info("Uncompressed file already locally available");
                Generic.compressGZIP(new File(fileName.replaceAll(".gz$","")), new File(fileName));
            }
            // if gz file exists
            if (new File(fileName).exists()) {
                log.info("Compressed file already locally available");
            } else {
                // Get study identifier (IF according to ENA format we can download the study as a whole and upload to irods
                String sample = IRODS.sampleLookup.get(fileName);
                if (sample.matches("DISABLED_FOR_NOW_[DE]RS[0-9]+")) {
                    // Download via ENA entire project not enabled due to network issues from ENA's side.
                    log.info("Attempt to download via ENA");
                    String enagroupget = commandOptions.enaBrowserTools + "/python3/enaGroupGet";
                    String identifier = fileName.split("_")[0];
                    String[] command = {enagroupget,"-f","fastq","-d", identifier, identifier};
                    try {
                        ExecCommand execCommand = new ExecCommand(command);
                        if (execCommand.getExit() == 0) {
                            log.info("Files downloaded");
                        }
                    } catch (IOException e) {
                        log.error("ENA execution failed");
                    }
                } else if (new File(App.commandOptions.sratoolkit).exists()) {
                    String sratoolkit = App.commandOptions.sratoolkit + "/bin/fasterq-dump";
                    String identifier = fileName.split("_")[0];
                    String[] command = {sratoolkit, "--force", identifier};
                    try {
                        log.info("Attempt to download " + identifier + " via SRA");
                        ExecCommand execCommand = new ExecCommand(command);
                        if (execCommand.getExit() == 0) {
                            log.info("File downloaded");
                        }
                    } catch (IOException e) {
                        log.error("SRA execution failed");
                    }
                } else {
                    throw new IOException("SRA toolkit not found " + App.commandOptions.sratoolkit);
                }
                // Compress file from download or previous download
                Generic.compressGZIP(new File(fileName.replaceAll(".gz$","")), new File(fileName));
            }

            Data.uploadIrodsFile(connection, new File(fileName), new File(landingzone + "/data/sra/"+ assayType + "/" + fileName));
            // Delete fileName
            Files.delete(new File(fileName).toPath());
            // Delete compressed fileName
            fileName = fileName.replaceAll(".gz$","");
            Files.delete(new File(fileName).toPath());

            return true;
        }

        log.error("Failed to find " + fileName + " in " + landingzone);
        return false;
    }

    public static HashSet<String> findFile(Connection connection, String fileName, String supposedPath) throws GenQueryBuilderException, JargonException, MalformedURLException { // throws GenQueryBuilderException, JargonQueryException, JargonException {
        log.debug("Searching for " + fileName + " in " + supposedPath);
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.EQUAL, fileName);
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(2);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;
        try {
            irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
            TimeUnit.SECONDS.sleep(0);
        } catch (JargonException | InterruptedException | JargonQueryException e) {
            e.printStackTrace();
            return new HashSet<>();
        }

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        // If no hits are found
        if (irodsQueryResultSetResults.size() == 0) {
            log.error(fileName + " not found in " + supposedPath);
            return new HashSet<>();
        } else {
            // if one hit or more is found collect all paths
            HashSet<String> paths = new HashSet<>();
            for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
                log.info(fileName + " found in irods at " + path);
                if (path.startsWith(supposedPath)) {
                    paths.add(path);
                } else {
//                    System.err.println(path + " not in " + supposedPath);
                }
            }

            // if more than one hit found
            if (paths.size() > 1) {
                HashSet<String> checksums = new HashSet<>();
                for (String path : paths) {
                    String shakey = Generic.getSHA256(path);
                    checksums.add(shakey);
                    paths.add(path);
                }
                if (checksums.size() == 1) {
                    log.debug("Multiple identical files detected at " + StringUtils.join(paths, "\n"));
                    return paths;
                }
                log.error("More than one result found without an identical checksum");
                return new HashSet<>();
            }
            return paths;
        }
    }

    /**
     * @param irodsFile file to obtain size from in megabytes
     * @param connection irods connection object
     * @return the file size in megabytes
     * @throws GenQueryBuilderException
     * @throws JargonException
     * @throws JargonQueryException
     */
    public static double fileSize(Connection connection, IRODSFile irodsFile) throws GenQueryBuilderException, JargonException, JargonQueryException {
        // DATA_SIZE
        String folder = irodsFile.getParentFile().getAbsolutePath();
        folder = folder.replaceAll("/$","");
        String file = irodsFile.getName();

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.EQUAL, folder);
        // Skip files found in trash
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.EQUAL, file);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_SIZE);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(2);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet;
        irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);
        double size = Long.parseLong(irodsQueryResultSet.getResults().get(0).getColumn(0)) / (1024.0 * 1024);
        log.info("Size of " + folder + "/" + file +" "+ size);
        return size;
    }

}