package nl.munlock.datamanager.irods;

import nl.munlock.datamanager.App;
import nl.munlock.datamanager.ExecCommand;
import nl.munlock.datamanager.Generic;
import nl.munlock.datamanager.GenomeSync;
import nl.munlock.ontology.domain.*;
import nl.wur.ssb.RDFSimpleCon.RDFFormat;
import nl.wur.ssb.RDFSimpleCon.ResultLine;
import nl.wur.ssb.RDFSimpleCon.api.Domain;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.*;
import org.apache.log4j.Logger;
import org.irods.jargon.core.exception.DataNotFoundException;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.UserTypeEnum;
import org.irods.jargon.core.pub.*;
import org.irods.jargon.core.pub.domain.User;
import org.irods.jargon.core.pub.domain.UserGroup;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.*;
import org.jermontology.ontology.JERMOntology.domain.*;
import org.purl.ppeo.PPEO.owl.domain.observation_unit;
import org.schema.domain.Person;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

import static nl.munlock.datamanager.App.*;
import static nl.munlock.datamanager.Generic.getProperty;

public class IRODS {
    private static final Logger log = Generic.getLogger(IRODS.class, App.debug);
    //    static String project;
    private static Domain domain;
    private static Project project;
    private static Investigation investigation;
    public static HashMap<String, String> sampleLookup;
    public static HashMap<String, String> fileLookup = new HashMap<>();
    private static ArrayList<File> files = new ArrayList<File>();

    public static void make() throws Exception {
        String base = "/" + connection.irodsAccount.getZone() + "/projects";

        log.info("Load rdf file");

        Data.downloadFile(connection, commandOptions.turtle);
        domain = new Domain("file://." + commandOptions.turtle);

        ResultLine projectResult = domain.getRDFSimpleCon().runQuerySingleRes("getProjects.txt", false);

        String projectIRI = projectResult.getIRI("project");

        project = domain.make(Project.class, projectIRI);

        // Basic checks
        initialChecks();

        // Make project environment
        makeProjectEnvironment(base);

        // IRI correction
        iriCorrection();

        // Double check the files and obtain read information, might need some optimization
        fileCheck();

        // Obtain project again as URL might have changed due to naming standardizations
        projectResult = domain.getRDFSimpleCon().runQuerySingleRes("getProjects.txt", false);
        projectIRI = projectResult.getIRI("project");
        project = domain.make(Project.class, projectIRI);

        // Check for null's in the rdf resource
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("checkNulls.txt", true).iterator();
        boolean error = false;
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String subject = resultLine.getIRI("subject");
            String predicate = resultLine.getIRI("predicate");
            log.error("Null detected for " + subject + " " + predicate);
            if (commandOptions.donotcheck) {
                if (predicate.endsWith("base64") || predicate.endsWith("sha256")) {

                } else {
                    error = true;
                }
            } else {
                error = true;
            }
        }

        if (error) {
            String message = "Nulls detected... please revise the code";
            if (commandOptions.debug)
                System.err.println(message);
            else
                throw new Exception(message);
        }

        // Upload to each investigation in this rdf file
        // Obtain project again as URL might have changed due to naming standardizations
        projectResult = domain.getRDFSimpleCon().runQuerySingleRes("getProjects.txt", false);
        projectIRI = projectResult.getIRI("project");
        project = domain.make(Project.class, projectIRI);

        for (Investigation investigation1 : project.getAllHasPart()) {
            // Update and upload project RDF file and xlsx file
            String location = "." + commandOptions.turtle.getAbsolutePath() + ".update.ttl";
            log.info("Generating " + location + " " + domain.getRDFSimpleCon().getModel().size() + " triples");
            new File(location).getParentFile().mkdirs();
            domain.save(location, RDFFormat.TURTLE);

            Data.uploadIrodsFile(connection, new File(location), new File(base + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation1.getIdentifier() + "/" + commandOptions.turtle.getName()));
            // Data.uploadIrodsFile(connection, commandOptions.turtle, new File(base + "/PRJ_" + project.getIdentifier() + "/" + commandOptions.excel.getName()));

            // Copy the excel file in a final step
            IRODSFile excel = connection.fileFactory.instanceIRODSFile(commandOptions.turtle.toString().replaceAll(".ttl$", ".xlsx"));
            if (excel.exists()) {
                log.info("Copying excel file to main project folder");
                Data.copyIrodsFile(connection, excel.getAbsoluteFile(), new File(base + "/PRJ_" + project.getIdentifier() + "/INV_" + investigation1.getIdentifier() + "/" + commandOptions.turtle.getName().replaceAll(".ttl$", ".xlsx")));
            } else {
                log.info("Copy of excel file failed");
            }
        }
    }

    private static void initialChecks() throws Exception {
        // Internal new connection due to many queries?
        Connection connection = new Connection();

        // Get all study identifiers with the filename
        Iterator<ResultLine> resultLineIterator = domain.getRDFSimpleCon().runQuery("getSampleFromWithFileName.txt", true).iterator();
        sampleLookup = new HashMap<>();
        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String sampleId = resultLine.getLitString("id");
            String fileName = resultLine.getLitString("name");
            sampleLookup.put(fileName, sampleId);
        }

        // Obtain all files and paths
        ArrayList<String> supposedPaths = new ArrayList<>();
        // When it is amplicon data it needs to be in the landingzone folder but does not have to be in a project folder
        // As it can be part of multiple projects?
        // For everything else this should be stored in the project folder

        String landingZone = "/" + connection.irodsAccount.getZone() + "/landingzone/projects/P%_" + project.getIdentifier() + "/";
        supposedPaths.add(landingZone);

        // Always add the project path
        supposedPaths.add("/" + connection.irodsAccount.getZone() + "/projects/P%" + project.getIdentifier() + "/%/unprocessed%");

        for (String supposedPath : supposedPaths) {
            log.info("Finding files in " + supposedPath);
            try {
                files.addAll(Data.listFiles(connection, connection.fileFactory.instanceIRODSFile(supposedPath)));
            } catch (JargonQueryException pe) {
                log.info(pe.getMessage());
            }
            System.err.println(files.size() + " found in irods");
        }

        // Get all files and check if they are in irods
        log.info("Searching for files, reconnecting to irods every nth file");
        resultLineIterator = domain.getRDFSimpleCon().runQuery("getFiles.txt", true).iterator();
        boolean permanent = false;

        // If no hits found something is wrong...
        if (!resultLineIterator.hasNext())
            permanent = false;

        while (resultLineIterator.hasNext()) {
            ResultLine resultLine = resultLineIterator.next();
            String file = resultLine.getIRI("file");
            String fileType = resultLine.getIRI("fileType");
            String assayType = resultLine.getIRI("assayType");
            String found;
            String fileName;
            HashSet<String> fileNames = new HashSet<>();
            if (fileType.endsWith("SingleSequenceDataSet")) {
                SingleSequenceDataSet single = domain.make(SingleSequenceDataSet.class, file);
                fileName = single.getName();
                fileNames.add(fileName);
                found = Data.findFile(connection, fileName, files);
            } else if (fileType.endsWith("PairedSequenceDataSet")) {
                nl.munlock.ontology.domain.PairedSequenceDataSet paired = domain.make(PairedSequenceDataSet.class, file);
                fileName = paired.getName();
                fileNames.add(fileName);
                found = Data.findFile(connection, fileName, files);
                // Check paired entry
                fileName = paired.getPaired().getName();
                fileNames.add(fileName);
                if (found != null) {
                    found = Data.findFile(connection, fileName, files);
                }
            } else {
                Data_sample data_sample = domain.make(Data_sample.class, file);
                fileName = data_sample.getName();
                found = Data.findFile(connection, fileName, files);
                System.err.println(found);
               //  throw new Exception("Unknown data format detected " + fileType);
            }

            // If no path was returned
            if (found == null) {
                for (String name : fileNames) {
                    log.info("Checking online resources for " + name);
                    boolean status;
                    if (name.matches("[DES]RR[0-9]+_[12].fastq.gz")) {
                        log.info("ENA identifier detected");
                        // boolean status = Data.checkOnlineResources(connection, name, landingZone, assayType.replaceAll(".*/", ""));
                        status = true;
                    } else {
                        log.info("File not found: " + name);
                        status = false;
                    }

                    if (!status) {
                        permanent = true;
                    }
                }
            } else {
                fileLookup.put(fileName, found);
            }
        }
        if (permanent) {
            if (commandOptions.debug) {
                log.warn("Not all files found from this dataset");
            } else {
                throw new Exception("Not all files found from this dataset");
            }
        }
    }

    /**
     * @throws Exception
     */
    private static void iriCorrection() throws Exception {
        ArrayList<Statement> removeStatements = new ArrayList<>();
        ArrayList<Statement> addStatements = new ArrayList<>();

        HashMap<String, String> replaceBy = new HashMap<>();

        // For each project, replace the project iri by a new project iri
        Iterable<ResultLine> results = domain.getRDFSimpleCon().runQuery("getProjects.txt", true);
        for (ResultLine result : results) {
            Project project = domain.make(Project.class, result.getIRI("project"));
            // replaceBy.put(result.getIRI("project"), "http://" + commandOptionsProject.host + project.getPath());

            for (Investigation investigation : project.getAllHasPart()) {
                // replaceBy.put(investigation.getResource().getURI(), "http://" + commandOptionsProject.host + investigation.getPath());

                for (Study study : investigation.getAllHasPart()) {
                    // replaceBy.put(study.getResource().getURI(), "http://" + commandOptionsProject.host + study.getPath());
                    for (org.purl.ppeo.PPEO.owl.domain.observation_unit observation_unit : study.getAllHasPart()) {
                        for (Sample sample : observation_unit.getAllHasPart()) {
                            for (Assay assay : sample.getAllHasPart()) {
                                // TODO CHECK SHOULD WE STILL REPLACE ALL IRIS OR COME UP WITH SOMETHING MORE UNREADABLE?
                                // replaceBy.put(assay.getResource().getURI(), "http://" + commandOptionsProject.host + assay.getPath());
                                for (Data_sample data_sample : assay.getAllHasPart()) {
                                    // String sha256 = Generic.checksum(new File(fastqFile), Hashing.sha256());
                                    // connection.accessObjectFactory.getDataObjectChecksumUtilitiesAO(connection.irodsAccount).computeChecksumOnDataObject();
                                    // Change data sample by "ni:///sha-256;"
                                    // connection.fileFactory.instanceIRODSFile(data_sample.getFilePath()).
                                    // log.info(data_sample.getFileName());
                                    replaceBy.put(data_sample.getResource().getURI(), "ni:///sha-256;" + data_sample.getSha256());
                                }
                            }
                        }
                    }
                }
            }
        }

        // Subject replacements
        StmtIterator statements = domain.getRDFSimpleCon().getModel().listStatements();
        while (statements.hasNext()) {
            Statement statement = statements.nextStatement();
            String subject = statement.getSubject().getURI();
            if (replaceBy.containsKey(subject)) {
                Statement newStatement = domain.getRDFSimpleCon().getModel().createStatement(domain.getRDFSimpleCon().getModel().createResource(replaceBy.get(subject)), statement.getPredicate(), statement.getObject());

                if (!newStatement.getSubject().toString().matches(statement.getSubject().toString())) {
                    removeStatements.add(statement);
                    addStatements.add(newStatement);
                }
            }
        }

        for (Statement statement : addStatements) {
            domain.getRDFSimpleCon().getModel().add(statement);
        }

        for (Statement statement : removeStatements) {
            domain.getRDFSimpleCon().getModel().remove(statement);
        }

        // Reset for object replacement
        removeStatements = new ArrayList<>();
        addStatements = new ArrayList<>();

        // Object statement replacement for proper connections
        statements = domain.getRDFSimpleCon().getModel().listStatements();
        while (statements.hasNext()) {
            Statement statement = statements.nextStatement();
            if (statement.getObject().isURIResource()) {
                String object = statement.getObject().asResource().getURI();
                if (replaceBy.containsKey(object)) {
                    Statement newStatement = domain.getRDFSimpleCon().getModel().createStatement(statement.getSubject(), statement.getPredicate(), domain.getRDFSimpleCon().getModel().createResource(replaceBy.get(object)));
                    // File matching from assay which do not need renaming
                    if (!newStatement.getObject().toString().matches(statement.getObject().toString())) {
                        removeStatements.add(statement);
                        addStatements.add(newStatement);
                    }
                }
            }
        }

        for (Statement statement : addStatements) {
            domain.getRDFSimpleCon().getModel().add(statement);
        }

        for (Statement statement : removeStatements) {
            domain.getRDFSimpleCon().getModel().remove(statement);
        }
    }

    private static void assaySnippets(Assay assay) throws Exception {
        log.debug("Creating assay snippet for " + assay.getLogicalPath());
        // Making the RDF snippet for the assay set as long as it is not amplicon library data
        if (!assay.getClassTypeIri().endsWith("AmpliconLibraryAssay")) {
            // Get the sample corresponding to this assay as well
            ResultLine resultLine = domain.getRDFSimpleCon().runQuerySingleRes("getSampleFromAssay.txt", true, assay.getResource().getURI());
            Sample sample = domain.make(Sample.class, resultLine.getIRI("sample"));

            // Making an Assay specific RDF database with a snapshot of the data
            Domain assayDomain = new Domain("");
            assayDomain.getRDFSimpleCon().setNsPrefix("schema", "http://schema.org/");
            assayDomain.getRDFSimpleCon().setNsPrefix("unlock", "http://m-unlock.nl/ontology/");
            assayDomain.getRDFSimpleCon().setNsPrefix("jerm", "http://jermontology.org/ontology/JERMOntology#");
            assayDomain.getRDFSimpleCon().setNsPrefix("irods", "http://unlock-icat.irods.surfsara.nl/");

            Resource resource = domain.getRDFSimpleCon().getModel().getResource(sample.getResource().getURI());
            StmtIterator stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                // A skip step for assay predicate if not matching the original assay
                if (statement.getPredicate().getURI().contains("http://jermontology.org/ontology/JERMOntology#hasPart")) {
                    // Check if iri matches
                    if (statement.getObject().toString().contains(assay.getResource().getURI())) {
                        assayDomain.getRDFSimpleCon().getModel().add(statement);
                    }
                } else {
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            resource = domain.getRDFSimpleCon().getModel().getResource(assay.getResource().getURI());
            stmtIterator = resource.listProperties();
            while (stmtIterator.hasNext()) {
                Statement statement = stmtIterator.next();
                assayDomain.getRDFSimpleCon().getModel().add(statement);
            }

            for (Data_sample data_sample : assay.getAllHasPart()) {
                resource = domain.getRDFSimpleCon().getModel().getResource(data_sample.getResource().getURI());

                stmtIterator = resource.listProperties();
                while (stmtIterator.hasNext()) {
                    Statement statement = stmtIterator.next();
                    assayDomain.getRDFSimpleCon().getModel().add(statement);
                }
            }

            String assayFile = assay.getIdentifier() + ".ttl";
            assayDomain.save(assayFile, RDFFormat.TURTLE);
            assayDomain.close();

            Data.uploadIrodsFile(connection, new File(assay.getIdentifier() + ".ttl"), new File(assay.getLogicalPath() + "/" + assay.getIdentifier() + ".ttl"));
            new File(assayFile).delete();
        }
    }

    /**
     * Parses all objects
     *
     * @throws Exception
     */
    private static void fileCheck() throws Exception {
        // E.g. obtain read information etc...
        Iterable<ResultLine> results = domain.getRDFSimpleCon().runQuery("getProjects.txt", true);
        for (ResultLine result : results) {
            Project project = domain.make(Project.class, result.getIRI("project"));
            for (Investigation investigation : project.getAllHasPart()) {
                for (Study study : investigation.getAllHasPart()) {
                    for (observation_unit observation_unit : study.getAllHasPart()) {
                        for (Sample sample : observation_unit.getAllHasPart()) {
                            for (Assay assay : sample.getAllHasPart()) {
                                for (Data_sample dataset : assay.getAllHasPart()) {
                                    // Only analysing FASTQ file types for now
                                    if (dataset.getFileFormat().toString().matches("(FASTQ|format_1930)")) {
                                        // SequenceDataSet sequenceDataSet = (SequenceDataSet) dataset;
                                        // If amplicon data analyse anyway ignoring restrictions
                                        if (!commandOptions.donotcheck) {
                                            String irodsFile = dataset.getContentUrl();
                                            if (irodsFile.startsWith("http"))
                                                irodsFile = new URL(dataset.getContentUrl()).getPath();
                                            Generic.getSequenceFileInfo(dataset, connection.fileFactory.instanceIRODSFile(irodsFile));
                                        }
                                        // log.info("Finished setting sequence information for: " + dataset.getFilePath());
                                    } else {
                                        log.warn("This file type is not integrated yet: " + dataset.getFileFormat());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Creates the project folder environment
     *
     * @param base
     * @throws Exception
     */
    private static void makeProjectEnvironment(String base) throws Exception {
        log.info("Creating project environment " + project.getIdentifier());

        base = base + "/PRJ_" + project.getIdentifier();
        base = validate(base);

        // This program should not be able to make the Root project folder.
        project.setLogicalPath(base);

        // Check if project path exists, otherwise create it
        if (!connection.fileFactory.instanceIRODSFile(project.getLogicalPath()).exists()) {
            log.info("Creating project " + project.getLogicalPath());
            connection.fileFactory.instanceIRODSFile(project.getLogicalPath()).mkdirs();
        }

        // Add metadata
        Generic.addMetadataCollection("type", "Project", "", project.getLogicalPath());

        for (org.jermontology.ontology.JERMOntology.domain.Investigation investigation : project.getAllHasPart()) {
            makeInvestigationEnvironment(investigation, base);
        }
    }

    private static String validate(String base) {
        String characters = "!\"#$&'()*,;<=>?[\\]^`{|}~";
        for (char c : characters.toCharArray()) {
            base = base.replace(String.valueOf(c), "");
        }
        return base;
    }

    /**
     * Creates irods users
     *
     * @throws JargonException
     */
    private static void setUsers() throws JargonException {
        // Create a group
        UserGroupAO userGroupAO = connection.accessObjectFactory.getUserGroupAO(connection.irodsAccount);

        // Making project_investigation group
        String groupName = project.getIdentifier() + "_" + investigation.getIdentifier();
        UserGroup ug = userGroupAO.findByName(groupName);
        if (ug == null) {
            log.info("Making group: " + project.getIdentifier());
            ug = new UserGroup();
            ug.setUserGroupName(groupName);
            ug.setZone(connection.irodsAccount.getZone());
            userGroupAO.addUserGroup(ug);
            log.info("Group made: " + ug.getUserGroupName());
        } else {
            log.info("Group: " + groupName);
        }

        UserAO userAO = connection.accessObjectFactory.getUserAO(connection.irodsAccount);

        if (investigation.getAllHasContributor().size() == 0) {
            throw new JargonException("No contacts found");
        }

        // Add group to investigation
        log.info("Adding group to project as read only");
        CollectionAO collectionAO = connection.accessObjectFactory.getCollectionAO(connection.irodsAccount);
        // Add group to project but not recursive reading
        log.info("Assigning " + groupName + " to " + project.getLogicalPath());
        collectionAO.setAccessPermissionRead(connection.irodsAccount.getZone(), project.getLogicalPath(), groupName, false);
        // Add group to investigation but with recursive reading
        log.info("Assigning " + groupName + " to " + investigation.getLogicalPath());
        if (collectionAO.getPermissionForCollection(investigation.getLogicalPath(), groupName, "").getPermissionNumericValue() == -1) {
            collectionAO.setAccessPermissionRead(connection.irodsAccount.getZone(), investigation.getLogicalPath(), groupName, true);
        }
        // Set inheritance recursively
        collectionAO.setAccessPermissionInherit(connection.irodsAccount.getZone(), investigation.getLogicalPath(), true);

        // Add technicians to project as owners
        UserGroup technicians = userGroupAO.findByName("technicians");
        log.info("Assigning " + technicians + " to " + project.getLogicalPath());
        if (collectionAO.getPermissionForCollection(project.getLogicalPath(), technicians.getUserGroupName(), "").getPermissionNumericValue() == -1) {
            collectionAO.setAccessPermissionOwn(connection.irodsAccount.getZone(), project.getLogicalPath(), technicians.getUserGroupName(), true);
        }


        for (Person person : project.getAllResearcher()) {
            String mbox = person.getEmail().toLowerCase().replaceAll("mailto:", "");
            try {
                userAO.findByName(mbox);
            } catch (DataNotFoundException e) {
                log.info("Creating user: " + mbox);
                User user = new User();
                user.setName(mbox);
                user.setUserType(UserTypeEnum.RODS_USER);
                userAO.addUser(user);
            }


            boolean inGroup = false;
            for (User listUserGroupMember : userGroupAO.listUserGroupMembers(groupName)) {
                if (listUserGroupMember.getName().contains(mbox)) {
                    inGroup = true;
                }
            }
            if (!inGroup) {
                log.info("Adding user " + mbox + " to group " + ug.getUserGroupName());
                userGroupAO.addUserToGroup(groupName, mbox, null);
            } else {
                log.info("User " + mbox + " already in group " + ug.getUserGroupName());
            }
        }
    }

    private static void makeInvestigationEnvironment(Investigation investigationX, String base) throws Exception {
        investigation = investigationX;

        log.info("Creating investigation environment " + investigation.getIdentifier());

        base = base + "/INV_" + investigation.getIdentifier();
        base = validate(base);

        investigation.setLogicalPath(base);

        IRODSFile irodsInvestigation = connection.fileFactory.instanceIRODSFile(base);
        irodsInvestigation.mkdirs();

        // Add metadata
        Generic.addMetadataCollection("type", "Investigation", "", base);

        // Creating users and assign them to the investigation and project
        setUsers();

        // Creating the reference folder in the investigation of the project
        if (!connection.fileFactory.instanceIRODSFile(investigation.getLogicalPath() + "/references").exists()) {
            log.debug("Creating reference folder " + investigation.getLogicalPath());
            connection.fileFactory.instanceIRODSFile(investigation.getLogicalPath() + "/references").mkdirs();
        }

        for (org.jermontology.ontology.JERMOntology.domain.Study study : investigation.getAllHasPart()) {
            makeStudyEnvironment(study, base);
        }
    }

    private static void makeStudyEnvironment(Study study, String base) throws Exception {
        log.info("Creating study environment " + study.getIdentifier());

        base = base + "/STU_" + study.getIdentifier();
        base = validate(base);

        study.setLogicalPath(base);

        IRODSFile irodsStudy = connection.fileFactory.instanceIRODSFile(base);
        irodsStudy.mkdirs();

        // Add metadata
        Generic.addMetadataCollection("type", "Study", "", base);

        int counter = study.getAllHasPart().size();
        for (observation_unit observation_unit : study.getAllHasPart()) {
            counter--;
            makeObservationEnvironment(observation_unit, base, counter);
        }
    }

    private static void makeObservationEnvironment(observation_unit observation_unit, String base) throws Exception {
        makeObservationEnvironment(observation_unit, base, -1);
    }

    private static void makeObservationEnvironment(observation_unit observation_unit, String base, int counter) throws Exception {
        log.info("Creating observation unit environment number " + counter + " " + observation_unit.getIdentifier());

        base = base + "/OBS_" + observation_unit.getIdentifier();
        base = validate(base);

        observation_unit.setLogicalPath(base);

        IRODSFile irodsObservationUnit = connection.fileFactory.instanceIRODSFile(base);
        irodsObservationUnit.mkdirs();

        // Add metadata
        Generic.addMetadataCollection("type", "ObservationUnit", "", base);

        for (Sample sample : observation_unit.getAllHasPart()) {
            makeSampleEnvironment(sample, observation_unit, base);
        }
    }

    private static void makeSampleEnvironment(Sample sample, observation_unit observation_unit, String base) throws Exception {
        // Create sample folder
        log.info("Creating sample environment " + sample.getIdentifier());

        base = base + "/SAM_" + sample.getIdentifier();
        base = validate(base);

        // sample.setLogicalPath(base);

        IRODSFile irodsSample = connection.fileFactory.instanceIRODSFile(base);
        irodsSample.mkdirs();

        // Add metadata
        Generic.addMetadataCollection("type", "Sample", "", base);

        // Create assays
        for (Assay assay : sample.getAllHasPart()) {
            makeAssayEnvironment(assay, sample, base);
        }
    }

    private static void makeAssayEnvironment(Assay assay, Sample sample, String base) throws Exception {
        log.info("Creating assay environment " + assay.getIdentifier());
        getReference(assay);
        // Get library source
        String librarySource = getProperty(assay.getResource().getModel(), assay.getResource().getURI(), "http://unlock-icat.irods.surfsara.nl/library_source").next().toString().toLowerCase();
        System.err.println("LIBRARY SOURCE " + librarySource);

        List<? extends String> types = assay.getAllAdditionalType();
        for (String type : types) {
            System.err.println("Analyzing type " + type);
            if (type.endsWith("/Amplicon")) {
                // Set path etc for the library if not set
                // AmpliconLibraryAssay library = ((AmpliconAssay) assay).getLibrary();
                // Check if amplicon assay exists remotely before demultiplexing...
                boolean hasLibrary = checkLibrary(assay);

                if (hasLibrary) {
                    connection.counter++;
                    if (connection.counter > 15) {
                        connection.close();
                        connection = new Connection();
                    }
                    assay = demultiplex(assay, sample, base);
                    if (assay == null) {
                        return;
                    }
                }

                assay.setLogicalPath(base + "/amplicon/ASY_" + assay.getIdentifier());

                // Copy it to the right location
                // Specify path to where it should be copied to...
                // P_Project / I_Investigation / S_Study / AssayType / A_Assay / unprocessed
                for (Data_sample dataset : assay.getAllHasPart()) {
                    // TODO Perform file search to allow manual online synchronisation using other scripts
                    File outputFilePath = new File(base + "/amplicon/ASY_" + assay.getIdentifier() + "/unprocessed/" + dataset.getName());
                    IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(outputFilePath.getAbsolutePath());
                    if (!irodsFile.exists()) {
                        // Obtain from ENA if matches
                        String landingZone = "/" + connection.irodsAccount.getZone() + "/landingzone/projects/PRJ_" + project.getIdentifier() + "/";
                        String assayType = assay.getClassTypeIri().replaceAll(".*/", "");
                        String location = landingZone + "/data/sra/" + assayType + "/" + dataset.getName();
                        if (!connection.fileFactory.instanceIRODSFile(location).exists()) {
                            boolean status = Data.checkOnlineResources(connection, dataset.getName(), landingZone, assayType);
                            if (!status) {
                                // throw new Exception("Download of public file failed " +dataset.getName());
                                log.error("Download of public file failed " + dataset.getName());
                                continue;
                            }
                        }
                        String irodsFilePath = getIRODSFilePath(dataset.getName());
                        Data.copyIrodsFile(connection, new File(irodsFilePath), outputFilePath);
                    }
                    // Obtain hash from irods
                    dataset.setBase64(Generic.getBase64(irodsFile));
                    dataset.setSha256(Generic.getSHA256(irodsFile));
                    dataset.setContentSize(irodsFile.length());
                    dataset.setContentUrl("https://" + connection.irodsAccount.getHost() + outputFilePath.getAbsolutePath());
                    // Obtain sequence information
                    String irodsFilePath = dataset.getContentUrl();
                    if (irodsFilePath.startsWith("http"))
                        irodsFilePath = new URL(dataset.getContentUrl()).getPath();

                    Generic.getSequenceFileInfo(dataset, connection.fileFactory.instanceIRODSFile(irodsFilePath));
                }
            } else if (type.endsWith("Illumina") || type.endsWith("PacBio") || type.endsWith("Nanopore")) {
                assay.setLogicalPath(base + "/" + librarySource + "/ASY_" + assay.getIdentifier());
                System.err.println(assay.getLogicalPath());
                // Copy it to the right location
                // Specify path to where it should be copied to...
                // P_Project / I_Investigation / S_Study / AssayType / A_Assay / unprocessed
                for (Data_sample dataset : assay.getAllHasPart()) {
                    File outputFilePath = new File(base + "/" + librarySource + "/ASY_" + assay.getIdentifier() + "/unprocessed/" + dataset.getName());
                    IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(outputFilePath.getAbsolutePath());
                    if (!irodsFile.exists()) {
                        if (fileLookup.containsKey(irodsFile.getName())) {
                            String irodsFilePath = getIRODSFilePath(dataset.getName());
                            Data.copyIrodsFile(connection, new File(irodsFilePath), outputFilePath);
                        } else {
                            // Obtain from ENA if matches
                            String landingZone = "/" + connection.irodsAccount.getZone() + "/landingzone/projects/PRJ_" + project.getIdentifier() + "/";
                            boolean status = Data.checkOnlineResources(connection, dataset.getName(), landingZone, assay.getClassTypeIri().replaceAll(".*/", ""));
                            if (!status) {
                                String message = "Download of public file failed " + dataset.getName();
                                log.error(message);
                                System.err.println(message);
                                continue;
                                // throw new Exception("Download of public file failed " + dataset.getName());
                            }
                        }
                        // Perform the copy?
                        String irodsFilePath = getIRODSFilePath(dataset.getName());
                        Data.copyIrodsFile(connection, new File(irodsFilePath), outputFilePath);
                    }
                    dataset.setContentUrl("https://" + connection.irodsAccount.getHost() + outputFilePath.getAbsolutePath());
                    dataset.setBase64(Generic.getBase64(irodsFile));
                    dataset.setSha256(Generic.getSHA256(irodsFile));
                }
//            }
//            else if (type.endsWith("/RNASeqAssay")) {
//                assay.setLogicalPath(base + "/rna/ASY_" + assay.getIdentifier());
//                // Copy it to the right location
//                // Specify path to where it should be copied to...
//                // P_Project / I_Investigation / S_Study / AssayType / A_Assay / unprocessed
//                for (Data_sample dataset : assay.getAllHasPart()) {
//                    File outputFilePath = new File(base + "/rna/ASY_" + assay.getIdentifier() + "/unprocessed/" + dataset.getName());
//                    IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(outputFilePath.getAbsolutePath());
//                    if (!irodsFile.exists()) {
//                        String irodsFilePath = getIRODSFilePath(dataset.getName());
//                        Data.copyIrodsFile(connection, new File(irodsFilePath), outputFilePath);
//                    }
//                    dataset.setContentUrl("https://" + connection.irodsAccount.getHost() + outputFilePath.getAbsolutePath());
//                    dataset.setBase64(Generic.getBase64(irodsFile));
//                    dataset.setSha256(Generic.getSHA256(irodsFile));
//                }
            } else {
                throw new Exception("Not supported yet: " + type);
            }

            // Create the TTL snippet
            assaySnippets(assay);
            Generic.addMetadataCollection("type", "Assay", "", assay.getLogicalPath());
        }
    }

    private static boolean checkLibrary(Assay assay) {
        for (Data_sample data_sample : assay.getAllHasPart()) {
            // Obtain schema:description TODO update ontology
            NodeIterator nodeIterator = domain.getRDFSimpleCon().getObjects(data_sample.getResource().getURI(), "schema:description");
            if (nodeIterator.next().asLiteral().getString().contains("Library"));
            return true;
        }
        return false;
    }

    /**
     * Obtains reference information from assay
     * Downloads reference if public GCA number and generates YAML file for kubernetes to build STAR/BOWTIE
     *
     * @param assay
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     * @throws XPathExpressionException
     * @throws JargonException
     * @throws GenQueryBuilderException
     * @throws JargonQueryException
     */
    private static String getReference(Assay assay) throws Exception {
        log.debug("Creating reference environment");
        // TODO make project specific reference database when not public data is used
        String projectPath = "/" + connection.irodsAccount.getZone() + "/references/genomes/";
        connection.fileFactory.instanceIRODSFile(projectPath).mkdirs();

        Data_sample reference = (Data_sample) assay.getReference();
        if (reference != null) {
            if (reference.getName().matches("^GCA_[0-9]+(\\.\\d+)?$")) {
                // Check GCA number in iRODS otherwise run genomesync for this specific GCA number...?
                String filePath = GenomeSync.sync_ena_gca(connection, commandOptions, reference.getName());
                reference.setContentUrl("https://" + connection.irodsAccount.getHost() + filePath);
                if (1 == 1)
                    return filePath;


                // OBSOLETE...

                // TODO check if file exists in irods
                log.info("GCA NUMBER DETECTED: " + reference.getName());
//                https://www.ebi.ac.uk/ena/browser/api/xml/GCA_900248155.1
                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                String url = "https://www.ebi.ac.uk/ena/browser/api/xml/" + reference.getName();
                log.info("Obtaining XML file from " + url);
                Document doc = db.parse(new URL(url).openStream());
                XPath xPath = XPathFactory.newInstance().newXPath();
                String expression = "/ASSEMBLY_SET/ASSEMBLY/ASSEMBLY_LINKS/ASSEMBLY_LINK/URL_LINK";
                NodeList nodeList = (NodeList) xPath.compile(expression).evaluate(doc, XPathConstants.NODESET);
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Node nNode = nodeList.item(i);

                    String label = nNode.getChildNodes().item(0).getNextSibling().getFirstChild().getTextContent();
                    String url_link = nNode.getChildNodes().item(1).getNextSibling().getNextSibling().getFirstChild().getTextContent();
                    connection.fileFactory.instanceIRODSFile(projectPath + "/" + label).mkdirs();

                    log.info(label + " " + url_link);

                    String referenceFileName = reference.getName();
                    if (url_link.endsWith(".dat.gz")) {
                        referenceFileName += ".dat.gz";
                        reference.setFileFormat(FileType.format_1927);
                    } else if (url_link.endsWith(".fasta.gz")) {
                        referenceFileName += ".fasta.gz";
                        reference.setFileFormat(FileType.format_1929);
                    } else {
                        throw new IOException("File extension not recognised");
                    }
                    if (!new File(referenceFileName).exists()) {
                        try (BufferedInputStream in = new BufferedInputStream(new URL(url_link).openStream());
                             FileOutputStream fileOutputStream = new FileOutputStream(referenceFileName)) {
                            byte dataBuffer[] = new byte[1024];
                            int bytesRead;
                            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                                fileOutputStream.write(dataBuffer, 0, bytesRead);
                            }
                        } catch (IOException e) {
                            // handle exception
                            log.error("Download of " + referenceFileName + " failed.");
                        }
                    }

                    // Upload
                    String referenceFolder = FilenameUtils.removeExtension(referenceFileName);
                    File remoteFile = new File(projectPath + "/" + referenceFolder + "/" + referenceFileName);
                    reference.setContentUrl("https://" + connection.irodsAccount.getHost() + projectPath + "/" + referenceFolder + "/" + referenceFileName);
                    if (!connection.fileFactory.instanceIRODSFile(projectPath + "/" + label + "/" + referenceFileName).exists()) {
                        log.info("Uploading " + referenceFileName);
                        Data.uploadIrodsFile(connection, new File(referenceFileName), remoteFile);
                    } else {
                        log.warn("File already exists remotely");
                    }
                }
            } else {
                log.info("Reference file detected, checking raw data reference folder");
                String referenceFolder = FilenameUtils.removeExtension(reference.getName());
                File remoteFile = new File(projectPath + "/" + referenceFolder + "/" + reference.getName());
                // Search for the name anywhere except projects from others?
                String irodsFilePath = getIRODSFilePath(reference.getName());
                Data.copyIrodsFile(connection, new File(irodsFilePath), remoteFile);
            }

        }

        String referencePath = "";
        return referencePath;
    }

    public static String checkFileExistsOnIrods(Connection connection, String fileName) throws GenQueryBuilderException, JargonException, JargonQueryException {
        log.info("Checking for " + fileName + " in irods");

        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "/" + connection.irodsAccount.getZone() + "/references/rdf/gbol/%");
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.LIKE, fileName);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit?
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(2);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
            String filePath = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
            log.info("File already in irods: " + filePath);
            return filePath;
        }
        return null;
    }


    /**
     * TODO this module can be improved by knowing which library files it already checked (saves a second per assay)
     *
     * @param assay
     * @param sample
     * @param base
     * @return
     * @throws Exception
     */
    private static Assay demultiplex(Assay assay, Sample sample, String base) throws Exception {
        String libraryName = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/libraryName").next().toString();

        // NGTAx demultiplex folder...?
        log.info("Starting demultiplexing on " + libraryName);

        // Get all data sets that have this library
        ArrayList<Data_sample> libraryAssayFiles = new ArrayList<>();
        for (Data_sample data_sample : assay.getAllHasPart()) {
            String description = domain.getRDFSimpleCon().getObjects(data_sample.getResource().getURI(), "schema:description").next().toString();
            if (description.contains("Library forward filename") || description.contains("Library reverse filename")) {
                libraryAssayFiles.add(data_sample);
            }
        }

        if (libraryAssayFiles.size() > 2) {
            log.error("Amplicon assay " + assay.getIdentifier());
            for (Data_sample libraryAssayFile : libraryAssayFiles) {
                log.error(libraryAssayFile.getName());
            }
            throw new Exception("Found more than 2 library files for one library");
        }

        // Obtain elements
        String forwardPrimer = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/forwardPrimer").next().toString();
        String reversePrimer = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/reversePrimer").next().toString();

        String folder = libraryName + "-" + forwardPrimer + "-" + reversePrimer + "/";

        File output = new File(folder.replaceAll("^_-_", ""));

        if (libraryAssayFiles.size() == 0) {
            log.error("No library assay file detected for " + libraryName);
            return null;
        } else if (libraryAssayFiles.size() == 2) {
            // Check if demultiplexed already exists in irods
            String name = new File(output + "/" + assay.getIdentifier()).getName(); // + "_f.fastq.gz").getName();
            File demultiplexedFwdName = new File(output + "/" + name + "_f.fastq.gz");
            File demultiplexedRevName = new File(output + "/" + name + "_r.fastq.gz");

            // Perform lookup in landingzone for demultiplexed files
            HashSet<String> pathsFwd = Data.findFile(connection, demultiplexedFwdName.getName(), "/" + connection.irodsAccount.getZone() + "/landingzone/");
            HashSet<String> pathsRev = Data.findFile(connection, demultiplexedRevName.getName(), "/" + connection.irodsAccount.getZone() + "/landingzone/");

            // Destination file name
            String fwdPath = base + "/amplicon/ASY_" + assay.getIdentifier() + "/unprocessed/" + demultiplexedFwdName.getName();
            String revPath = base + "/amplicon/ASY_" + assay.getIdentifier() + "/unprocessed/" + demultiplexedRevName.getName();

            // Destination irods object
            IRODSFile irodsFwdPath = connection.fileFactory.instanceIRODSFile(fwdPath);
            IRODSFile irodsRevPath = connection.fileFactory.instanceIRODSFile(revPath);

            // Perform the demultiplex preprocessing (set file paths etc) if assay was not found
            Data_sample libraryPairedSequenceDataSet1 = libraryAssayFiles.get(0);
            Data_sample libraryPairedSequenceDataSet2 = libraryAssayFiles.get(1);

            // Build mapping file... with all assay properties? TODO .. library file path currently not available...
            String expectedPath = "/" + connection.irodsAccount.getZone() + "/landingzone/%";
            String libraryPath1 = "";
            String libraryPath2 = "";
            try {
                libraryPath1 = getIRODSFilePath(libraryPairedSequenceDataSet1.getName(), expectedPath);
                libraryPath2 = getIRODSFilePath(libraryPairedSequenceDataSet2.getName(), expectedPath);

                for (Data_sample data_sample : assay.getAllHasPart()) {
                    if (data_sample.getName().contains(libraryPairedSequenceDataSet1.getName())) {
                        // Set library logical path based on the forward file
                        // library.setLogicalPath(new File(libraryPath1).getParent());
                        // Set file information for the library
                        libraryPairedSequenceDataSet1.setContentUrl("https://" + connection.irodsAccount.getHost() + libraryPath1);
                        libraryPairedSequenceDataSet1.setSha256(Generic.getSHA256(libraryPath1));
                        libraryPairedSequenceDataSet1.setBase64(Generic.getBase64(libraryPath1));

                    }
                    if (data_sample.getName().contains(libraryPairedSequenceDataSet2.getName())) {
                        // Set file information for the library
                        libraryPairedSequenceDataSet2.setContentUrl("https://" + connection.irodsAccount.getHost() + libraryPath2);
                        libraryPairedSequenceDataSet2.setSha256(Generic.getSHA256(libraryPath2));
                        libraryPairedSequenceDataSet2.setBase64(Generic.getBase64(libraryPath2));
                    }
                }
            } catch (Exception e) {
                // If pathsFwd or rev is empty no amplicon files have been found
                if (pathsFwd.size() == 0 || pathsRev.size() == 0) {
                    System.err.println(irodsFwdPath.exists() + " " + irodsFwdPath.getAbsolutePath());
                    System.err.println(irodsRevPath.exists() + " " + irodsRevPath.getAbsolutePath());
                    throw new JargonException(e.getMessage());
                }
            }


            if (irodsFwdPath.exists() && irodsRevPath.exists()) {
                log.debug("Demultiplexed files already on iRODS");
            } else if (pathsFwd.size() > 0 && pathsRev.size() > 0) {
                log.debug("Demultiplexed file detected in landingzone, copying it to final destination " + new File(fwdPath).getParent());
                IRODSFile irodsFwdLandingzoneFile = connection.fileFactory.instanceIRODSFile(pathsFwd.iterator().next());
                IRODSFile irodsRevLandingzoneFile = connection.fileFactory.instanceIRODSFile(pathsRev.iterator().next());

                DataTransferOperations dataTransferOperations = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataTransferOperations(connection.irodsAccount);
                dataTransferOperations.copy(irodsFwdLandingzoneFile, irodsFwdPath, null, null);
                dataTransferOperations.copy(irodsRevLandingzoneFile, irodsRevPath, null, null);
            } else {
                log.info("No demultiplex file found, starting demultiplexing");
                // When library was not yet demultiplexed... download library files and perform demultiplexing
                if (!output.exists()) {
                    // Downloading the libraries making sure there are no leftovers of a previous run
                    remove(new File(libraryPath1));
                    remove(new File(libraryPath2));

                    Data.downloadFile(connection, new File(libraryPath1));
                    Data.downloadFile(connection, new File(libraryPath2));

                    // For a given library obtain all assays, get the primers, barcodes, etc...
                    // Both files obtained... perform demultiplexing primer specific!
                    generateMappingFile(assay);

                    String[] commandArgs = {
                            "java",
                            "-jar",
                            commandOptions.ngtax,
                            "-demultiplex",
                            "-mapFile", "mapping_file.txt",
                            "-for_p", forwardPrimer,
                            "-rev_p", reversePrimer,
                            "-output", output.getName(),
                            "-fastQ", "." + libraryPath1 + "," + "." + libraryPath2};

                    // nl.wur.ssb.NGTax.App.main(commandArgs);
                    String command = StringUtils.join(commandArgs, " ");
                    log.debug(command);
                    ExecCommand execCommand = new ExecCommand(commandArgs);
                    if (execCommand.getExit() == 0) {
                        log.debug("Finished demultiplexing, removing library files");
                        // Remove library files
                        remove(new File("." + libraryPath1));
                        remove(new File("." + libraryPath2));
                    } else {
                        log.error("Demultiplexing failed");
                        log.error(execCommand.getError());
                        // Remove library files
                        remove(new File("." + libraryPath1));
                        remove(new File("." + libraryPath2));
                        return null;
                    }
                } else {
                    log.info("Skipping demultiplexing as it is already executed");
                }

                // Upload to iRODS... get Full path
                Data.uploadIrodsFile(connection, demultiplexedFwdName, new File(fwdPath));
                Data.uploadIrodsFile(connection, demultiplexedRevName, new File(revPath));
            }

            // These files are from the same library but can belong to different studies / projects / whatever...
            try {
                if (assay.getAllHasPart().size() > 2) {
                    throw new Exception("Files already there? " + assay.getResource().getURI());
                }
            } catch (RuntimeException e) {
            }

            // Build metadata... obtain from assay ttl file?
            log.debug("Creating metadata amplicon assay object");
            // Forward & Reverse
            String fwdFilePath = base + "/amplicon/ASY_" + assay.getIdentifier() + "/unprocessed/" + demultiplexedFwdName.getName();
            String sha256 = Generic.getSHA256(fwdFilePath);
            PairedSequenceDataSet fwdDataset = domain.make(PairedSequenceDataSet.class, "ni:///sha-256;" + sha256);

            fwdDataset.setName(demultiplexedFwdName.getName());
            fwdDataset.setContentUrl("https://" + connection.irodsAccount.getHost() + fwdFilePath);
            fwdDataset.setBase64(Generic.getBase64(fwdDataset.getContentUrl()));
            fwdDataset.setSha256(sha256);
            fwdDataset.setFileFormat(FileType.format_1930);
            fwdDataset.setSeqPlatform(SequencingPlatform.Illumina);
            fwdDataset.setReadLength((long) 0);
            fwdDataset.setReads((long) 0);
            fwdDataset.setBases((long) 0);
            fwdDataset.setContentSize(demultiplexedFwdName.length());

            String revFilePath = base + "/amplicon/ASY_" + assay.getIdentifier() + "/unprocessed/" + demultiplexedRevName.getName();
            sha256 = Generic.getSHA256(revFilePath);

            PairedSequenceDataSet revDataset = domain.make(PairedSequenceDataSet.class, "ni:///sha-256;" + sha256);

            revDataset.setName(demultiplexedRevName.getName());
            revDataset.setContentUrl("https://" + connection.irodsAccount.getHost() + revFilePath);
            revDataset.setBase64(Generic.getBase64(fwdDataset.getContentUrl()));
            revDataset.setSha256(sha256);
            revDataset.setFileFormat(FileType.format_1930);
            revDataset.setSeqPlatform(SequencingPlatform.Illumina);
            revDataset.setReadLength((long) 0);
            revDataset.setReads((long) 0);
            revDataset.setBases((long) 0);
            revDataset.setContentSize(demultiplexedRevName.length());

            domain.disableCheck();
            fwdDataset.setPaired(revDataset);
            revDataset.setPaired(fwdDataset);
            domain.enableCheck();

            assay.addHasPart(fwdDataset);
            assay.addHasPart(revDataset);
            assay.setLogicalPath(base + "/amplicon/ASY_" + assay.getIdentifier() + "");

            sample.addHasPart(assay);

            // URI checksum check if they are equal
            if (fwdDataset.getResource().getURI().equals(revDataset.getResource().getURI())) {
                // If either of the datasets is > 0 we throw an error
                if (fwdDataset.getContentSize() > 0 || revDataset.getContentSize() > 0) {
                    log.error("Forward and reverse file contain the same sha-key\n" +
                            fwdDataset.getName() + "\n" +
                            revDataset.getName());
                    return null;
                }
            }
            return assay;
        } else {
            SingleSequenceDataSet singleSequenceDataSet = (SingleSequenceDataSet) libraryAssayFiles.get(0);
            getIRODSFilePath(singleSequenceDataSet.getName());
            throw new Exception("Single end has not been done yet...");
        }
    }

    private static void remove(File file) {
        if (!file.exists()) {

        }
        while (file.exists()) {
            file.delete();
        }
    }

    public static String getIRODSFilePath(String fileName) throws GenQueryBuilderException, JargonQueryException, JargonException, MalformedURLException {
        return getIRODSFilePath(fileName, null);
    }

    private static String getIRODSFilePath(String fileName, String expectedPath) throws GenQueryBuilderException, JargonException, JargonQueryException, MalformedURLException {
        log.debug("Searching for " + fileName + " in " + expectedPath);
        IRODSGenQueryBuilder queryBuilder = new IRODSGenQueryBuilder(true, null);

        // Generalised to landingzone folder, check later if its ENA or Project
//        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "%landingzone%");
        // IF pattern does not match ENA... add project search field?
        // IF expectedPath is given
        if (expectedPath != null) {
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, expectedPath);
        } else {
            queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.LIKE, "/" + connection.irodsAccount.getZone() + "/landingzone/projects/PRJ_" + project.getIdentifier() + "%");
        }
        // Skips trash folder
        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_COLL_NAME, QueryConditionOperators.NOT_LIKE, "/" + connection.irodsAccount.getZone() + "/trash/%");

        queryBuilder.addConditionAsGenQueryField(RodsGenQueryEnum.COL_DATA_NAME, QueryConditionOperators.EQUAL, fileName.trim());
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_COLL_NAME);
        queryBuilder.addSelectAsGenQueryValue(RodsGenQueryEnum.COL_DATA_NAME);

        // Set limit to 2 but should only return 1 file
        IRODSGenQueryFromBuilder query = queryBuilder.exportIRODSQueryFromBuilder(99);

        IRODSGenQueryExecutor irodsGenQueryExecutor = connection.accessObjectFactory.getIRODSGenQueryExecutor(connection.irodsAccount);
        IRODSQueryResultSet irodsQueryResultSet = irodsGenQueryExecutor.executeIRODSQuery(query, 0);

        List<IRODSQueryResultRow> irodsQueryResultSetResults = irodsQueryResultSet.getResults();

        if (irodsQueryResultSetResults.size() == 0) {
            if (expectedPath == null) {
                throw new JargonQueryException("No results found with " + "COLL_NAME like '/" + connection.irodsAccount.getZone() + "/landingzone/projects/PRJ_" + project.getIdentifier() + "%" + " and DATA_NAME = '" + fileName + "'");
            } else {
                throw new JargonQueryException("No results found with " + "COLL_NAME like '" + expectedPath + "' and DATA_NAME = '" + fileName + "'");
            }
        } else {
            // if one hit or more is found collect all paths
            HashSet<String> paths = new HashSet<>();
            for (IRODSQueryResultRow irodsQueryResultSetResult : irodsQueryResultSetResults) {
                String path = irodsQueryResultSetResult.getColumn(0) + "/" + irodsQueryResultSetResult.getColumn(1);
                if (expectedPath == null) {
                    paths.add(path);
                } else if (!path.startsWith(expectedPath.replaceAll("%", ""))) {
                    log.info("Ignoring file as path of file is not in the expected folder\n imv " + path + " " + expectedPath);
                } else {
                    paths.add(path);
                }
            }

            // if more than one hit found
            if (paths.size() == 1) {
                return paths.iterator().next();
            }

            HashSet<String> checksums = new HashSet<>();
            for (String path : paths) {
                String shakey = Generic.getSHA256(path);
                checksums.add(shakey);
                paths.add(path);
            }

            if (checksums.size() == 1) {
                log.debug("Multiple identical files detected at " + org.apache.commons.lang.StringUtils.join(paths, "\n"));
                return paths.iterator().next();
            }
            log.error("More than one result found without an identical checksum");
            return null;
        }
    }

    private static void generateMappingFile(Assay ampliconAssayReference) throws Exception {
        String libraryName = domain.getRDFSimpleCon().getObjects(ampliconAssayReference.getResource().getURI(), "http://m-unlock.nl/ontology/libraryName").next().toString();

        PrintWriter printWriter = new PrintWriter("mapping_file.txt");
        printWriter.println("#sampleID\tforwardBarcodeSequence\treverseBarcodeSequence\tLibraryNumber\tDirection\tLibraryName");

        Iterable<ResultLine> ampliconAssays = domain.getRDFSimpleCon().runQuery("getAssayIDsFromLibrary.txt", true, libraryName);

        for (ResultLine resultLine : ampliconAssays) {
            Assay assay = domain.make(Assay.class, resultLine.getIRI("assay"));

            // Obtain elements
            String forwardPrimer = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/forwardPrimer").next().toString();
            String reversePrimer = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/reversePrimer").next().toString();

            String forwardPrimerReference = domain.getRDFSimpleCon().getObjects(ampliconAssayReference.getResource().getURI(), "http://m-unlock.nl/ontology/forwardPrimer").next().toString();
            String reversePrimerReference = domain.getRDFSimpleCon().getObjects(ampliconAssayReference.getResource().getURI(), "http://m-unlock.nl/ontology/reversePrimer").next().toString();

            if (!forwardPrimer.contains(forwardPrimerReference)) {
                log.info("Assay from same library with different primer detected " + assay.getIdentifier() + " vs reference " + ampliconAssayReference.getIdentifier());
                continue;
            }

            if (reversePrimer != null && !reversePrimer.contains(reversePrimerReference)) {
                log.info("Assay from same library with different primer detected " + assay.getIdentifier() + " vs reference " + ampliconAssayReference.getIdentifier());
                continue;
            }

            String fwdBarcode = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/forwardBarcode").next().toString();
            String revBarcode = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/reverseBarcode").next().toString();

            String sampleID = assay.getIdentifier();
            String libraryNumber = "1";
            String direction = "p";

            // String libraryName = domain.getRDFSimpleCon().getObjects(assay.getResource().getURI(), "http://m-unlock.nl/ontology/libraryName").next().toString();
            printWriter.println(sampleID + "\t" + fwdBarcode + "\t" + revBarcode + "\t" + libraryNumber + "\t" + direction + "\t" + libraryName);
        }
        printWriter.close();
    }
}