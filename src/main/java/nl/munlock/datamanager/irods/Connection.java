package nl.munlock.datamanager.irods;

import nl.munlock.datamanager.Generic;
import nl.munlock.datamanager.App;
import org.apache.log4j.Logger;
import org.irods.jargon.core.connection.ClientServerNegotiationPolicy;
import org.irods.jargon.core.connection.IRODSAccount;
import org.irods.jargon.core.connection.SettableJargonProperties;
import org.irods.jargon.core.connection.SettableJargonPropertiesMBean;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.protovalues.ChecksumEncodingEnum;
import org.irods.jargon.core.pub.IRODSAccessObjectFactory;
import org.irods.jargon.core.pub.IRODSFileSystem;
import org.irods.jargon.core.pub.io.IRODSFileFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.util.Map;
import java.util.Scanner;

import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REFUSE;
import static org.irods.jargon.core.connection.ClientServerNegotiationPolicy.SslNegotiationPolicy.CS_NEG_REQUIRE;

public class Connection {
    public final IRODSFileSystem irodsFileSystem;
    public final IRODSAccount irodsAccount;
    public final IRODSAccessObjectFactory accessObjectFactory;
    public final IRODSFileFactory fileFactory;
    private static final Logger log = Generic.getLogger(Connection.class, App.debug);
    public int counter = 0;


    public Connection() throws JargonException {

        if (new File("config.txt").exists()) {
            log.info("Getting authentication from file");
            setEnv();
        }

        // Initialize account object
        irodsAccount = IRODSAccount.instance(
                System.getenv("irodsHost"),
                Integer.parseInt(System.getenv("irodsPort")),
                System.getenv("irodsUserName"),
                System.getenv("irodsPassword"),
                "",
                System.getenv("irodsZone"),
                "");

        SettableJargonPropertiesMBean jargonProperties = new SettableJargonProperties();
        jargonProperties.setChecksumEncoding(ChecksumEncodingEnum.SHA256);

        // Sets SSL settings
        ClientServerNegotiationPolicy clientServerNegotiationPolicy = new ClientServerNegotiationPolicy();
        ClientServerNegotiationPolicy.SslNegotiationPolicy sslPolicy;
        if (System.getenv("irodsSSL").matches("CS_NEG_REQUIRE")) {
            sslPolicy = CS_NEG_REQUIRE;
        } else if (System.getenv("irodsSSL").matches("CS_NEG_REFUSE")) {
            sslPolicy = CS_NEG_REFUSE;
        } else {
            throw new JargonException("SSL policy not recognised: " + System.getenv("irodsSSL"));
        }

        clientServerNegotiationPolicy.setSslNegotiationPolicy(sslPolicy);
        irodsAccount.setClientServerNegotiationPolicy(clientServerNegotiationPolicy);

        irodsFileSystem = IRODSFileSystem.instance();

        accessObjectFactory = irodsFileSystem.getIRODSAccessObjectFactory();

        fileFactory = accessObjectFactory.getIRODSFileFactory(irodsAccount);
    }

    public void close() throws JargonException {
        log.debug("Closing irods connection");
        this.irodsFileSystem.close();
        this.accessObjectFactory.closeSession(this.irodsAccount);
    }

    public static void setEnv() {
        Scanner scanner;
        try {
            scanner = new Scanner(new File("config.txt"));
            while (scanner.hasNextLine()) {
                String[] line = scanner.nextLine().split("\\s+");
                setEnv(line[0], line[1]);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void setEnv(String key, String value) {
        try {
            Map<String, String> env = System.getenv();
            Class<?> cl = env.getClass();
            Field field = cl.getDeclaredField("m");
            field.setAccessible(true);
            Map<String, String> writableEnv = (Map<String, String>) field.get(env);
            writableEnv.put(key, value);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to set environment variable", e);
        }
    }
}
