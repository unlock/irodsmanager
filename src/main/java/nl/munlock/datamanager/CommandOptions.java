package nl.munlock.datamanager;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
// import nl.munlock.ontology.domain.ComputationalAssayType;
import nl.wur.ssb.RDFSimpleCon.api.Domain;

import java.io.File;
import java.time.LocalDateTime;

public class CommandOptions {

    @Parameter(names = {"--help", "-h", "-help"})
    public boolean help = false;

    // Default irods settings
//    @Parameter(names = {"--username", "-username"}, description = "Username for iRODS authentication (System variable: irodsUserName)")
//    public String username = System.getenv("irodsUserName");
//
//    @Parameter(names = {"--password","-password"}, description = "Password for iRODS authentication (System variable: irodsPassword)")
//    public String password = System.getenv("irodsPassword");
//
//    @Parameter(names = {"--host","-host"}, description = "Host for iRODS authentication (System variable: irodsHost)")
//    public String host = System.getenv("irodsHost");
//
//    @Parameter(names = {"--port","-port"}, description = "Port for iRODS authentication (System variable: irodsPort)")
//    public String port = System.getenv("irodsPort");
//
//    @Parameter(names = {"--zone","-zone"}, description = "Zone for iRODS authentication (System variable: irodsZone)")
//    public String zone = System.getenv("irodsZone");

    // Container for the triple store
//    public Domain domain;

//    @Parameter(names = {"--authentication","-authentication"}, description = "Authentication scheme for iRODS authentication, e.g. password (System variable: irodsAuthScheme)")
//    String authentication = System.getenv("irodsAuthScheme");

    @Parameter(names = {"-turtle"}, description = "Turtle file of the project", required = true)
    public File turtle;

    @Parameter(names = {"-debug"}, description = "Enable debug mode")
    public boolean debug;

    @Parameter(names = {"-donotcheck"}, description = "Skips obtaining read information from sequence files for the metadata")
    public boolean donotcheck = false;

    public String[] args;

    @Parameter(names = {"-enaBrowserTools"}, description = "Path to the enaBrowserTools application when reference files are used")
    public String enaBrowserTools = "/unlock/infrastructure/binaries/enaBrowserTools";

    @Parameter(names = {"-conversionTools"}, description = "Path to the Conversion.jar for EMBL/FASTA to RDF conversion")
    public String conversion = "/unlock/infrastructure/binaries/sapp/Conversion.jar";

    @Parameter(names = {"-ngtaxTools"}, description = "Path to the NGTax.jar for demultiplexing")
    public String ngtax = "/unlock/infrastructure/binaries/NG-Tax2/NGTax-2.1.66.jar";

    @Parameter(names = {"-sratoolkit"}, description = "Path to the SRA Toolkit for automagically downloading SRA files")
    public String sratoolkit = "/unlock/infrastructure/binaries/sra/sratoolkit";

    @Parameter(names = {"-graphdb"}, description = "GraphDB REST endpoint e.g. (http://192.0.2.1:7200/rest/repositories)")
    public String graphdb;

    public CommandOptions(String args[]) {
        try {
            JCommander jc = new JCommander(this);
            jc.parse(args);
            if(this.help)
                throw new ParameterException("Help required");
            this.args = args;
        } catch (ParameterException pe) {
            int exitCode = 64;
            if (this.help)
                exitCode = 0;

            System.out.println(pe.getMessage());
            new JCommander(this).usage();
            System.out.println("  * required parameter");
            System.out.println("Please set the following system variables irodsHost, irodsPort, irodsUserName, irodsPassword, irodsZone");
            System.exit(exitCode);
        }
    }
}
