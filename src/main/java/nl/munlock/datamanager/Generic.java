package nl.munlock.datamanager;

import htsjdk.samtools.fastq.FastqReader;
import htsjdk.samtools.fastq.FastqRecord;
import nl.munlock.datamanager.irods.Connection;
import nl.munlock.datamanager.irods.Data;
import nl.wur.ssb.RDFSimpleCon.ExecCommand;
import org.apache.commons.io.FileUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.log4j.*;
import org.irods.jargon.core.checksum.ChecksumValue;
import org.irods.jargon.core.exception.JargonException;
import org.irods.jargon.core.pub.CollectionAO;
import org.irods.jargon.core.pub.DataObjectAO;
import org.irods.jargon.core.pub.DataObjectChecksumUtilitiesAO;
import org.irods.jargon.core.pub.domain.AvuData;
import org.irods.jargon.core.pub.io.IRODSFile;
import org.irods.jargon.core.query.GenQueryBuilderException;
import org.irods.jargon.core.query.JargonQueryException;
import org.irods.jargon.core.query.MetaDataAndDomainData;
import org.jermontology.ontology.JERMOntology.domain.Data_sample;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import static nl.munlock.datamanager.App.connection;


public class Generic {
    static final Logger logger = getLogger(Generic.class, App.debug);
    static ArrayList<String> processed = new ArrayList<>();

    public static String getMD5(File file) throws IOException {
        try (InputStream is = Files.newInputStream(Paths.get(file.getPath()))) {
            return org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
        }
    }

    public static ArrayList<File> getFromENA(CommandOptions commandOptions, String fileName) throws IOException, JargonException, GenQueryBuilderException, JargonQueryException {
        // List the files
        ArrayList<File> files = new ArrayList<>();

        // Check if fileName in /tempZone/landingzone/ENA/fileName
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile("/" + connection.irodsAccount.getZone() + "/landingzone/ENA/" + fileName);
        if (irodsFile.isDirectory()) {
            logger.info("Folder already exists in iRODS");
            files = Data.listFiles(connection, irodsFile);
        } else {
            logger.info("Obtain data from ENA " + fileName);
            // Run the data retrieval application
            String command = "python3 ./binaries/enaBrowserTools/python3/enaDataGet.py -f fastq " + fileName;
            ExecCommand execCommand = new ExecCommand(command);
            if (execCommand.getExit() > 0) {
                logger.error(execCommand.getOutput());
                logger.error(execCommand.getError());
                throw new IOException("Execution failed of: " + command);
            }

            for (File listFile : FileUtils.listFiles(new File(fileName), null, true)) {
                File destination = new File("/" + connection.irodsAccount.getZone() + "/landingzone/ENA/" + listFile.toString());
                Data.uploadIrodsFile(connection, listFile, destination);
                files.add(destination);
            }
            // Delete local directory
            logger.info("Removing local folder " + fileName);
            FileUtils.deleteDirectory(new File(fileName));
        }
        return files;
    }

    public static String getBase64(IRODSFile irodsFile) throws JargonException {
        DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
        ChecksumValue checksumValue = dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(irodsFile.getAbsolutePath());
        // Todo not sure if this works and it will receive a null for checksum
        if (checksumValue == null) {
            checksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
        }
        return checksumValue.getBase64ChecksumValue();
    }

    public static String getSHA256(IRODSFile irodsFile) throws JargonException {
        DataObjectChecksumUtilitiesAO dataObjectChecksumUtilitiesAO = App.connection.irodsFileSystem.getIRODSAccessObjectFactory().getDataObjectChecksumUtilitiesAO(connection.irodsAccount);
        // TODO there is an issue with too many checksum checks
        if (!irodsFile.exists()) {
            connection.close();
            connection = new Connection();
        }
        ChecksumValue checksumValue = dataObjectChecksumUtilitiesAO.retrieveExistingChecksumForDataObject(irodsFile.getAbsolutePath());
        // Todo not sure if this works and it will receive a null for checksum
        if (checksumValue == null) {
            try {
                checksumValue = dataObjectChecksumUtilitiesAO.computeChecksumOnDataObject(irodsFile);
            } catch (JargonException e) {
                logger.error("Failed to obtain checksum for " + irodsFile);
                throw new JargonException(e.getMessage());
            }
        }
        return checksumValue.getHexChecksumValue();
    }

    public static String getSHA256(String irodsFilePath) throws JargonException, MalformedURLException {
        if (irodsFilePath.startsWith("http")) {
            irodsFilePath = new URL(irodsFilePath).getPath();
        }
        IRODSFile irodsFile = App.connection.fileFactory.instanceIRODSFile(irodsFilePath);
        return getSHA256(irodsFile);
    }

    public static String getBase64(String irodsFilePath) throws JargonException, MalformedURLException {
        if (irodsFilePath.startsWith("http")) {
            irodsFilePath = new URL(irodsFilePath).getPath();
        }
        IRODSFile irodsFile = App.connection.fileFactory.instanceIRODSFile(irodsFilePath);
        return getBase64(irodsFile);
    }

    public static void getSequenceFileInfo(Data_sample dataSample, IRODSFile irodsFile) throws JargonException, IOException, GenQueryBuilderException, JargonQueryException {
        // Check for AVU information
        DataObjectAO dataObjectAO = connection.accessObjectFactory.getDataObjectAO(connection.irodsAccount);
        List<MetaDataAndDomainData> metaDataAndDomainDataList = dataObjectAO.findMetadataValuesForDataObject(irodsFile);
        HashSet<String> metadatacheck = new HashSet<>();
        // TODO
        System.err.println("TODO");
//        for (MetaDataAndDomainData metaDataAndDomainData : metaDataAndDomainDataList) {
//            AvuData avu = metaDataAndDomainData.asAvu();
//            if (avu.getAttribute().matches("reads")) {
//                dataSample.setReads(Long.valueOf(avu.getValue()));
//                metadatacheck.add("reads");
//            }
//            if (avu.getAttribute().matches("bases")) {
//                dataSample.setBases(Long.valueOf(avu.getValue()));
//                metadatacheck.add("bases");
//            }
//            if (avu.getAttribute().matches("readLength")) {
//                dataSample.setReadLength(Long.valueOf(avu.getValue()));
//                // temp bug fix
//                if (dataSample.getReadLength() != 0)
//                    metadatacheck.add("readLength");
//            }
//        }

        if (metadatacheck.size() == 3) {
            logger.debug("Sequence file information obtained from irods");
            return;
        }

        File localFile = new File("." + irodsFile.getPath());

        // Check if it is available under ./*/<filename>
        String fileName = irodsFile.getName();
        Collection files = FileUtils.listFiles(new File("."), null, true);

        for (Iterator iterator = files.iterator(); iterator.hasNext(); ) {
            File file = (File) iterator.next();
            if (file.getName().equals(fileName)) {
                // TODO checksum test?
                logger.info("File found locally " + file.getAbsolutePath());
                localFile = file;
                break;
            }
        }

        // fileSize in megabytes
        double fileSize = Data.fileSize(connection, irodsFile);

        // If not found download it
        if (!localFile.exists()) {
            if (fileSize > 1) {
                logger.warn("Skipping all > 1mb remove me when needed");
                logger.warn("Skipping sequence information for files larger than 1024 mb size is " + Data.fileSize(connection, irodsFile));
                return;
            }
            Data.downloadFile(connection, new File(irodsFile.getPath()));
        }

        if (processed.contains(localFile.getAbsolutePath())) {
            logger.warn("Skipping sequence information as file is already analysed " + localFile.getAbsolutePath());
            return;
        }

        processed.add(localFile.getAbsolutePath());

        Reader reader;

        if (irodsFile.getName().endsWith(".gz")) {
            // GZIP file...
            logger.debug("Testing " + localFile);
            InputStream gzipStream = new GZIPInputStream(new FileInputStream(localFile));
            reader = new InputStreamReader(gzipStream);
        } else {
            BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(localFile));
            reader = new InputStreamReader(bufferedInputStream);
        }

        BufferedReader br = new BufferedReader(reader);
        FastqReader fastqReader = new FastqReader(irodsFile.getAbsoluteFile(), br);

        long maxReadLength = 0;
        long bases = 0;
        long reads = 0;
        String message;
        while (fastqReader.hasNext()) {
            reads = reads + 1;
            // Have encountered funny files in the past (incomplete quality / empty sequences)
            try {
                FastqRecord fastqRecord = fastqReader.next();
                if (reads % 1000 == 0)
                    System.err.print(reads + "\r");
                bases = bases + fastqRecord.getReadLength();
                if (maxReadLength < fastqRecord.getReadLength()) {
                    maxReadLength = fastqRecord.getReadLength();
                }
            } catch (htsjdk.samtools.SAMException e) {
                message = e.getMessage();
                logger.error(message);
                if (message.contains("ZLIB input stream")) {
                    logger.error(irodsFile.getName() + " failed");
                    fastqReader.close();
                    break;
                }
            }
        }

        logger.info("Read length of " + maxReadLength + " and " + bases + " bases and " + reads + " reads");

        System.err.println("TODO");
//        dataSample.setReadLength(maxReadLength);
//        dataSample.setBases(bases);
//        dataSample.setReads(reads);

        // Add metadata to irods
        addMetadataDataObject( "reads", String.valueOf(reads), "", irodsFile.getAbsolutePath());
        addMetadataDataObject( "readLength", String.valueOf(maxReadLength), "", irodsFile.getAbsolutePath());
        addMetadataDataObject( "bases", String.valueOf(bases), "", irodsFile.getAbsolutePath());
    }

    /**
     * Logger initialization with debug option
     *
     * @param debug boolean if debug mode should be enabled
     * @return
     */
    public static Logger getLogger(Class clazz, boolean debug) {
        ConsoleAppender console = new ConsoleAppender();
        String PATTERN = "%d %-5p [%c{1}] %m%n %l ";
        console.setLayout(new PatternLayout(PATTERN));
        console.setThreshold(Level.DEBUG);
        console.activateOptions();

        Logger.getRootLogger().removeAllAppenders();
        Logger.getRootLogger().addAppender(console);

        Logger logger = Logger.getLogger(clazz);

        logger.setLevel(Level.INFO);

        if (debug) {
            logger.setLevel(Level.DEBUG);
        }

        FileAppender fa = new FileAppender();
        fa.setName("iRODS Logger");
        fa.setFile("runner.log");
        fa.setLayout(new PatternLayout("%d %-5p [%c{1}] %m%n %l "));
        fa.setThreshold(Level.INFO);
        if (debug)
            fa.setThreshold(Level.DEBUG);

        fa.setAppend(true);
        fa.activateOptions();
        org.apache.log4j.Logger.getRootLogger().addAppender(fa);

        return logger;
    }

    public static void addMetadataCollection(String name, String value, String unit, String collection) throws JargonException {
        AvuData avuData = AvuData.instance(name, value, unit);
        CollectionAO collectionAO = connection.accessObjectFactory.getCollectionAO(connection.irodsAccount);
        collectionAO.deleteAVUMetadata(collection, avuData);
        collectionAO.addAVUMetadata(collection, avuData);
    }

    public static void addMetadataDataObject(String name, String value, String unit, String dataObject) throws JargonException {
        AvuData avuData = AvuData.instance(name, value, unit);
        DataObjectAO dataObjectAO = connection.accessObjectFactory.getDataObjectAO(connection.irodsAccount);
        dataObjectAO.deleteAVUMetadata(dataObject, avuData);
        dataObjectAO.addAVUMetadata(dataObject, avuData);
    }

    public static int countLinesNew(String filename) throws IOException, JargonException {
        IRODSFile irodsFile = connection.fileFactory.instanceIRODSFile(filename);
        if (irodsFile.exists()) {
            Data.downloadFile(connection, new File(filename));
            filename = "." + filename;
        }

        InputStream is = new BufferedInputStream(new FileInputStream(new File(filename)));

        try {
            byte[] c = new byte[1024];

            int readChars = is.read(c);
            if (readChars == -1) {
                // bail out if nothing to read
                return 0;
            }

            // make it easy for the optimizer to tune this loop
            int count = 0;
            while (readChars == 1024) {
                for (int i = 0; i < 1024; ) {
                    if (c[i++] == '\n') {
                        ++count;
                        if (count % 10000 == 0) {
                            logger.info("Counted " + count + " linens");
                        }
                    }
                }
                readChars = is.read(c);
            }

            // count remaining characters
            while (readChars != -1) {
                for (int i = 0; i < readChars; ++i) {
                    if (c[i] == '\n') {
                        ++count;
                    }
                }
                readChars = is.read(c);
            }
            logger.info(count + " reads detected");
            return count == 0 ? 1 : count;
        } finally {
            is.close();
        }
    }

    public static void compressGZIP(File input, File output) throws IOException {
        try (GZIPOutputStream out = new GZIPOutputStream(new FileOutputStream(output))) {
            try (FileInputStream in = new FileInputStream(input)) {
                byte[] buffer = new byte[1024];
                int len;
                while ((len = in.read(buffer)) != -1) {
                    out.write(buffer, 0, len);
                }
            }
        }
    }

    public static NodeIterator getProperty(Model model, String subj, String pred) {
        Resource subjR = null;
        if (subj != null) {
            subjR = model.createResource(subj);
        }

        Property predR = null;
        if (pred != null) {
            predR = model.createProperty(pred);
        }

        return model.listObjectsOfProperty(subjR, predR);
    }
}
