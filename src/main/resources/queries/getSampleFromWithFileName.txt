PREFIX unlock: <http://m-unlock.nl/ontology/>
PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX schema: <http://schema.org/>
SELECT DISTINCT ?id ?name
WHERE {
    ?sample a jerm:Sample .
	?sample schema:identifier ?id .
    ?sample jerm:hasPart/jerm:hasPart ?file .
	?file a ?fileType .
    ?file schema:name ?name .
}