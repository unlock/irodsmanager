PREFIX up:<http://purl.uniprot.org/core/>
PREFIX taxon:<http://purl.uniprot.org/taxonomy/>
PREFIX rdfs:<http://www.w3.org/2000/01/rdf-schema#>
SELECT DISTINCT ?parentClass ?rank ?className
WHERE {
    VALUES ?taxon{taxon:%1$s}
    ?taxon up:scientificName ?name.
    {
        ?taxon up:rank ?rank .
        ?taxon up:scientificName ?className .
        BIND(?taxon AS ?parentClass)
    } union { ?taxon rdfs:subClassOf+ ?parentClass .
        ?parentClass up:rank ?rank .
        ?parentClass up:scientificName ?className .
    }
}